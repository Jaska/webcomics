# Transcript of Pepper&Carrot Episode 02 [ls]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 2: Pociones arcoíris

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|5|True|Glup
Sonido|6|True|Glup
Sonido|7|False|Glup
Texto|1|True|PELIGRO
Texto|2|False|CASA DE
Texto|3|True|BRUJA
Texto|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|7|False|tok|nowhitespace
Texto|1|False|DANZA
Texto|2|False|OCÉANO PROFUNDO
Texto|3|False|VIOLE(N)TA
Texto|4|False|ULTRA AZUL
Texto|5|False|ROS
Texto|6|False|ARMESÍ
Texto|8|False|NATURALEZA
Texto|9|False|AMARILLO
Texto|10|False|NARANJA SUPERIOR
Texto|11|False|DANZA DE FUEGO
Texto|12|False|OCÉANO PROFUNDO
Texto|13|False|VIOLE(N)TA
Texto|14|False|ULTRA AZUL
Texto|15|False|META ROSA
Texto|16|False|MAGENTA X
Texto|17|False|CARMESÍ

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|Glup
Sonido|2|True|Glup
Sonido|3|False|Glup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|Gulp
Sonido|2|False|Gulp
Sonido|3|True|Gulp
Sonido|4|False|Gulp
Sonido|5|True|Gulp
Sonido|6|False|Gulp
Sonido|20|False|m|nowhitespace
Sonido|19|True|m|nowhitespace
Sonido|18|True|M|nowhitespace
Sonido|7|True|S|nowhitespace
Sonido|8|True|P|nowhitespace
Sonido|9|True|l|nowhitespace
Sonido|10|False|urp !|nowhitespace
Sonido|11|True|S|nowhitespace
Sonido|12|True|S|nowhitespace
Sonido|13|True|S|nowhitespace
Sonido|14|True|P|nowhitespace
Sonido|15|True|l|nowhitespace
Sonido|16|True|a|nowhitespace
Sonido|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|True|B
Sonido|2|True|lup|nowhitespace
Sonido|3|True|B
Sonido|4|False|lup|nowhitespace
Sonido|7|True|up|nowhitespace
Sonido|6|True|pl|nowhitespace
Sonido|5|True|s
Sonido|10|True|up|nowhitespace
Sonido|9|True|pl|nowhitespace
Sonido|8|True|s
Sonido|13|False|up|nowhitespace
Sonido|12|True|pl|nowhitespace
Sonido|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|True|Este cómic web es de código abierto, este episodio fue financiado por 21 mecenas en:
Créditos|2|False|www.patreon.com/davidrevoy
Créditos|3|False|Muchas gracias a:
Créditos|4|False|hecho con Krita en GNU/Linux
