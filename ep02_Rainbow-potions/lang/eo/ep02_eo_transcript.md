# Transcript of Pepper&Carrot Episode 02 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 2a : La ĉielarkaj Sorĉotrinkaĵoj

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|5|True|Glugl
Sono|6|True|Glugl
Sono|7|False|Glugl
Skribaĵo|1|True|AVERTO
Skribaĵo|3|False|BIENO
Skribaĵo|2|True|SORĈA
Skribaĵo|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|7|False|Lok|nowhitespace
Skribaĵo|1|False|FAJRA
Skribaĵo|2|False|OCEANO
Skribaĵo|3|False|VI(TRI)OLO
Skribaĵo|4|False|BLUEGO
Skribaĵo|5|False|ROZ
Skribaĵo|6|False|USKO
Skribaĵo|8|False|NATURO
Skribaĵo|9|False|FLAVO
Skribaĵo|10|False|ORANĜA SURO
Skribaĵo|11|False|FAJRA DANC’
Skribaĵo|12|False|OCEANO
Skribaĵo|13|False|VI(TRI)OLO
Skribaĵo|14|False|BLUEGO
Skribaĵo|15|False|OMBRA ROZ’
Skribaĵo|16|False|FUĤSIO X
Skribaĵo|17|False|KREPUSKO

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|True|Glugl
Sono|2|True|Glugl
Sono|3|False|Glugl

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|True|Glut
Sono|2|False|Glut
Sono|3|True|Glut
Sono|4|False|Glut
Sono|5|True|Glut
Sono|6|False|Glut
Sono|21|False|...!|nowhitespace
Sono|20|True|z|nowhitespace
Sono|19|True|aŭ|nowhitespace
Sono|18|True|N|nowhitespace
Sono|8|True|Ŝ|nowhitespace
Sono|9|True|p|nowhitespace
Sono|10|True|r|nowhitespace
Sono|11|False|uc !|nowhitespace
Sono|12|True|G|nowhitespace
Sono|13|True|L|nowhitespace
Sono|14|True|U|nowhitespace
Sono|15|True|I|nowhitespace
Sono|16|True|Ĝ|nowhitespace
Sono|17|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|True|F
Sono|2|True|lu|nowhitespace
Sono|3|True|F
Sono|4|False|lu|nowhitespace
Sono|7|True|aŭd|nowhitespace
Sono|6|True|l|nowhitespace
Sono|5|True|p
Sono|10|True|d|nowhitespace
Sono|9|True|laŭ|nowhitespace
Sono|8|True|p
Sono|13|False|d|nowhitespace
Sono|12|True|laŭ|nowhitespace
Sono|11|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Atribuintaro|1|True|Tiu bildstrio estas libera verko. Ĉi tiu ĉapitro estis subtenita de 21 mecenatoj :
Atribuintaro|2|False|www.patreon.com/davidrevoy
Atribuintaro|3|False|Dankon al :
Atribuintaro|4|False|Desegnita per Krita sur GNU/Linux
Atribuintaro|5|False|Traduko : libre fan, Tirifto. Fasono : Navi, Tirifto.
