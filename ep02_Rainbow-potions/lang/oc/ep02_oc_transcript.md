# Transcript of Pepper&Carrot Episode 02 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 2 : Las pocions arcolan

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|5|True|Glo
Son|6|True|Glo
Son|7|False|Glo
Escritura|1|True|DANGIÈR
Escritura|3|False|PROPRIETAT PRIVADA
Escritura|2|True|MASCA
Escritura|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|7|False|tòc|nowhitespace
Escritura|1|False|FUÒC
Escritura|2|False|MAR SUAVA
Escritura|3|False|VIOLE(N)T
Escritura|4|False|ULTRÀ BLAU
Escritura|5|False|RÒ
Escritura|6|False|NGUIN
Escritura|8|False|NATURA
Escritura|9|False|JAUNE
Escritura|10|False|IRANGE NAUT
Escritura|11|False|FUÒC VIU
Escritura|12|False|MAR SUAVA
Escritura|13|False|VIOLE(N)T
Escritura|14|False|ULTRÀ BLAU
Escritura|15|False|METARÒSE
Escritura|16|False|MAGENTÀ X
Escritura|17|False|SANGUIN

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|Glo
Son|2|True|Glo
Son|3|False|Glo

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|Glo
Son|2|False|Glo
Son|3|True|Glo
Son|4|False|Glo
Son|5|True|Glo
Son|6|False|Glo
Son|20|False|m|nowhitespace
Son|19|True|m|nowhitespace
Son|18|True|M|nowhitespace
Son|7|True|S|nowhitespace
Son|8|True|P|nowhitespace
Son|9|True|l|nowhitespace
Son|10|False|orp !|nowhitespace
Son|11|True|S|nowhitespace
Son|12|True|S|nowhitespace
Son|13|True|S|nowhitespace
Son|14|True|P|nowhitespace
Son|15|True|l|nowhitespace
Son|16|True|o|nowhitespace
Son|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|B
Son|2|True|lup|nowhitespace
Son|3|True|B
Son|4|False|lup|nowhitespace
Son|7|True|ch|nowhitespace
Son|6|True|lo|nowhitespace
Son|5|True|p
Son|10|True|ch|nowhitespace
Son|9|True|lo|nowhitespace
Son|8|True|p
Son|13|False|ch|nowhitespace
Son|12|True|lo|nowhitespace
Son|11|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|True|Aquesta BD es open-source e aqueste episòdi foguèt sostengut per 21 mecènas sus
Crèdits|2|False|www.patreon.com/davidrevoy
Crèdits|3|False|Mercés a :
Crèdits|4|False|fait amb Krita sus GNU/Linux
