# Transcript of Pepper&Carrot Episode 36 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 36: Eit overraskande åtak

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|TILGJE DEG?!
Wasabi|2|False|Tullar du?!
Wasabi|3|False|KAST HENNE I FENGSEL!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Fsss j ...|nowhitespace
Pepar|2|True|Søren!
Pepar|3|False|Eg kan jo ikkje gjera noko her!
Pepar|4|False|Hersens magiske fengsel! Grrr!
Lyd|5|False|KLANK !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|At eg kunne vera så godtruande !
Shichimi|2|True|Hysssj, Pepar!
Shichimi|3|False|Demp deg.
Pepar|4|False|Kven der?!
Shichimi|5|True|Hysj, sa eg!
Shichimi|6|True|Kom hit.
Shichimi|7|False|Eg skal få deg fri.
Lyd|8|False|Ksjiii...
Pepar|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Eg er ... Eg er lei for det som hende ...
Shichimi|2|False|... slåstinga vår, veit du.
Shichimi|3|True|Eg ...
Shichimi|4|False|eg måtte gjera det.
Pepar|5|True|Eg skjønar. Ikkje tenk på det.
Pepar|6|False|Takk for at du er her.
Shichimi|7|False|Magien i cella er veldig sterk – du har verkeleg fått full pakke!
Pepar|8|False|Ha-ha!
Shichimi|9|False|Ikkje så høgt, elles oppdagar dei oss ...
Rotte|10|True|SLIKK
Rotte|11|True|SLIKK
Rotte|12|False|SLIKK
Shichimi|13|True|Men du,
Shichimi|14|True|det er endå ein grunn til at eg er her. Like etter seremonien vart eg innlemma i Wasabis indre sirkel,
Shichimi|15|False|og då fekk eg vita om planane hennar ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|16|False|KLANK !|nowhitespace
Lyd|18|False|KLINK !|nowhitespace
Lyd|19|False|KLANK !|nowhitespace
Lyd|20|False|KLINK !|nowhitespace
Lyd|21|False|KLANK !|nowhitespace
Shichimi|1|False|Det er forferdeleg, Pepar!
Shichimi|2|False|Wasabi ynskjer rett og slett å herska over alle dei andre magiskulane ...
Shichimi|3|False|Og ved morgongry dreg ho med ein hekse-hær til Kvalistad ...
Pepar|4|True|Å nei!
Pepar|5|True|Koriander!
Pepar|6|False|Og riket hennar!
Shichimi|7|True|Og Zombiah-magien!
Shichimi|8|False|Me må skunda oss og åtvara henne.
Shichimi|9|False|Ein drake med ryttar står klar oppe på plattforma for å hjelpa oss vekk.
Shichimi|10|False|Der – endeleg fekk eg opp låsen!
Lyd|11|False|Tsing!
Pepar|12|True|Bra jobba!
Pepar|13|False|Eg skal berre henta Gulrot og hatten min.
Rotte|14|False|Piiip!
Gulrot|15|False|FRE S !|nowhitespace
Rotte|17|False|Piiip!
Pepar|22|False|! ! !|nowhitespace
Shichimi|23|False|! ! !|nowhitespace
Vaktar|24|True|VAKT!
Vaktar|25|False|HJELP TIL!
Vaktar|26|False|EIN INNTRENGJAR PRØVER Å HJELPA FANGEN Å FLYKTA!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Men me klarte det nesten ...
Pepar|2|False|Ja, nesten ...
Pepar|3|False|Veit du forresten kvifor Wasabi har eit horn i sida til meg?
Shichimi|4|False|Ho er redd for de Kaosah-hekser, Pepar ...
Shichimi|5|True|Og spesielt for kjede-reaksjonane dykkar.
Shichimi|6|False|Ho meiner dei er ein stor trugsel mot planane hennar.
Pepar|7|True|Å, pfff ...
Pepar|8|False|Der treng ho iallfall ikkje uroa seg; eg har aldri heilt fått til å skapa ein einaste ein.
Shichimi|9|False|Har du ikkje?
Pepar|10|False|Nei, aldri, ha-ha-ha!
Shichimi|11|True|Hi-hi-hi!
Shichimi|12|False|Ho er verkeleg paranoid ...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|True|Adjutant!
Konge|2|False|Er du sikker på at dette er tempelet til denne heksa?
Adjutant|3|True|Temmeleg sikker, min herre!
Adjutant|4|False|Fleire av spionane våre har sett henne her i det siste.
Konge|5|True|Grrr...
Konge|6|False|Så det er her ho som trugar kringskunsten og tradisjonane våre , bur!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|False|La oss feira den nye alliansen vår ved å jamna heile tempelet med jorda.
Lyd|3|False|Klapp
Fiende|2|False|G odt sagt!
Hær|4|True|Bra!
Hær|5|True|Bra!
Hær|6|True|Bra!
Hær|7|True|Ja!
Hær|8|True|Heia!
Hær|9|True|Juurrr!
Hær|10|True|Hepp!
Hær|11|True|Jihaa!
Hær|12|True|Jaaah!
Hær|13|False|Hurra!
Konge|14|True|KATAPULTAR!
Konge|15|False|FYR!!!
Lyd|16|False|huUVOOOOOOOOOOO !|nowhitespace
Lyd|17|True|Vooosj!
Lyd|18|False|Vooosj!
Konge|19|False|TIL ÅÅÅTAAAK!!!
Pepar|20|True|Kva skjer?
Pepar|21|False|Eit åtak?!
Lyd|22|True|DRØNN !|nowhitespace
Lyd|23|False|DRØNN!
Shichimi|24|False|Kva?!
Lyd|25|False|KA-B O O M!|nowhitespace
Lyd|26|False|BRAK!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Host!
Pepar|2|False|Hark!
Pepar|3|False|Shichimi! Går det bra?
Shichimi|4|True|Ja, det gjekk fint.
Shichimi|5|True|Og du?
Shichimi|6|False|Og Gulrot?
Pepar|7|False|Bra med oss òg.
Pepar|8|True|Kva i alle dagar?!
Pepar|9|False|Eg ... kan ikkje tru det ...
Shichimi|10|True|Kva gjer alle krigarane her?!
Shichimi|11|True|Og med katapultar ?!
Shichimi|12|False|Kva vil dei oss?
Pepar|13|True|Ikkje godt å seia ...
Pepar|14|False|Men dei to der dreg eg iallfall kjensel på.
Shichimi|15|False|?
Shichimi|16|False|! ! !|nowhitespace
Shichimi|17|False|Torreya – her!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Åndene vera lova – du er trygg!
Lyd|3|False|Trykk
Torreya|4|True|Eg vart så redd då eg fekk høyra at dei hadde teke deg til fange!
Torreya|5|True|Og så dette åtaket!
Torreya|6|False|For eit kaos!
Shichimi|7|False|Det er så godt å sjå deg, Torreya.
Pepar|8|False|Du store ...
Pepar|9|False|Så drakeryttaren var kjærasten til Shichimi ...
Pepar|10|False|Tør ikkje tenkja på at eg nesten kvitta meg med henne der i hòla.
Pepar|11|False|Og dei to hærane må ha følgt etter meg.
Pepar|12|False|Hadde det ikkje vore for dei, hadde me framleis vore fengsla.
Pepar|13|False|Alt heng saman ...
Pepar|14|False|... Oi!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Kva er det med henne?
Shichimi|2|False|Pepar – går det bra?
Pepar|3|True|Ja, alt i orden.
Pepar|4|False|Eg har berre innsett noko.
Pepar|5|False|Alt som har skjedd, er – direkte eller indirekte – følgjer av mine handlingar, mine val ...
Pepar|6|False|... med andre ord: min kjedereaksjon!
Shichimi|7|True|Verkeleg?
Shichimi|8|False|Dette må du forklara for meg.
Torreya|9|True|Nok snakk – me er midt i eit slag.
Torreya|10|True|Me kan snakka om det på flyturen.
Torreya|11|False|Hopp på, Pepar!
Shichimi|12|True|Torreya har rett.
Shichimi|13|False|Me må koma oss til Kvalistad.
Pepar|14|False|Nei, vent litt.
Pepar|15|True|Hæren til Wasabi er i ferd med å gå til motåtak.
Pepar|16|False|Me kan ikkje berre la dei ta livet av kvarandre.
Pepar|17|False|Eg kjenner det er mitt ansvar å få slutt på denne kampen.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Men korleis?
Arra|2|True|Ja, korleis skal du få til det, vesle heks?
Arra|3|False|Eg kjenner at du ikkje har fått bygd opp nok rea enno.
Pepar|4|True|Det stemmer, men eg har ein trylleformel på lur som kanskje kan ordna alt.
Pepar|5|False|Eg treng berre lite grann av rea-en din for å kunna nå alle der nede.
Arra|6|True|Gje energi til ei heks?
Arra|7|True|Det er forbode!
Arra|8|False|Gløym det! Aldri!
Pepar|9|False|Vil du heller sjå eit blodbad her?
Torreya|10|True|Ver så snill, Arra. Gjer eit unntak. Jentene og drakane som slåst, høyrer jo til skulen vår, familien vår.
Torreya|11|False|Og det gjer du òg.
Shichimi|12|False|Ja, ver så snill, Arra!
Arra|13|True|PFF! Greitt!
Arra|14|False|Men ho gjer det på eige ansvar!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Fsj j j ! ! !|nowhitespace
Pepar|2|False|Oi! Å-å-å!
Pepar|3|False|Så det er slik drake-rea kjennest!
Shichimi|4|False|Skund deg, Pepar! Me har lite tid!
Pepar|5|False|Allus ...!
Pepar|6|False|Yuus ...!
Pepar|7|False|Needum ...!
Pepar|8|False|Est ...
Pepar|9|False|... LOVIUS!
Lyd|10|False|Dzziooo!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|5|True|Fuss!
Lyd|4|True|Dziiing!
Lyd|3|True|Hfhiii!
Lyd|2|True|Sjiii!
Lyd|1|True|Sjsss!
Lyd|6|True|Fuss!
Lyd|7|True|Dziiing!
Lyd|9|True|Sjiii!
Lyd|8|True|Hfhiii!
Lyd|10|False|Sjsss!
Pepar|11|False|Denne trylleformelen var første forsøket mitt på ein antikrigsformel.
Pepar|12|False|Han gjer fiendar om til vener ...
Pepar|13|False|... og vald om til kjærleik og medkjensle.
Shichimi|14|True|Oi!
Shichimi|15|True|Det ... Det er heilt utruleg, Pepar!
Shichimi|16|False|Dei har slutta å slåst!
Torreya|17|False|Formelen verkar!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Men kva er det som skjer?
Shichimi|2|False|Nokre av dei kyssar kvarandre?!
Torreya|3|False|Oi ... Her var det mange nye par!
Torreya|4|False|Hadde du rekna med dette, Pepar?
Pepar|5|False|Æh, eigentleg ikkje ... Det må ha vore drake-rea-en som førte til ein overdose kjærleik i formelen.
Torreya|6|False|Ha-ha! Dette slaget går nok rett inn i historiebøkene!
Shichimi|7|False|Tji-hi. Utan tvil!
Pepar|8|False|Å, så flautt!
Forteljar|9|False|– SLUTT –

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 1 036 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
Bidragsytarar|1|False|15. desember 2021 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini og Valvin. Omsetjing til nynorsk Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nicolas Artance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson. Programvare: Krita 5.0β og Inkscape 1.1 på Kubuntu Linux 20.04. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
