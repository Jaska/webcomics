# Transcript of Pepper&Carrot Episode 36 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Odcinek 36: Atak z zaskoczenia

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|PRZEPROSINY?!
Wasabi|2|False|Raczysz żartować!
Wasabi|3|False|WTRĄCIĆ JĄ DO LOCHU!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fs zzzz ...
Pepper|2|True|No nie!
Pepper|3|False|Moja moc tutaj nie działa!
Pepper|4|False|Przeklęte magiczne wię-zienie!
Sound|5|False|KLANG !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Jak mogłam być tak naiwna?!
Shichimi|2|True|Cicho, Pepper.
Shichimi|3|False|Nie krzycz.
Pepper|4|False|Kto tam jest?!
Shichimi|5|True|Ciiiicho! Nic nie mów.
Shichimi|6|True|Jestem tutaj.
Shichimi|7|False|Przyszłam cię uwolnić.
Sound|8|False|Ks z iii...
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Przepraszam za to, co się wydarzyło.
Shichimi|2|True|Chodzi mi o naszą kłótnię.
Shichimi|3|True|Ja
Shichimi|4|False|musiałam to zrobić.
Pepper|5|True|Nie martw się, wiem.
Pepper|6|False|Dziękuję, że przyszłaś.
Shichimi|7|False|Ta magiczna cela to nie w kij dmuchał. Nie szczędzili na ciebie środków.
Pepper|8|False|Haha!
Shichimi|9|False|Ciszej, bo nas usłyszą.
Rat|10|True|LAP
Rat|11|True|LAP
Rat|12|False|LAP
Shichimi|13|True|Posłuchaj,
Shichimi|14|True|kolejnym powodem, dla którego przybyłam, jest to, że Wasabi przyjęła mnie do wewnętrznego kręgu.
Shichimi|15|False|Dzięki temu poznałam jej plany.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|6|True|KLANG !
Shichimi|7|True|KLUNK !
Shichimi|8|False|KLANG !
Pepper|9|True|KLANK !
Pepper|10|True|KLANG !
Pepper|11|False|Są straszne, Pepper.
Shichimi|12|True|Wasabi chcę podbić wszystkie szkoły magii, tak po prostu.
Shichimi|13|True|Z armią wiedźm, wyruszy do Qualicity o poranku.
Shichimi|14|False|O nie!
Shichimi|15|False|Coriander!
Sound|16|False|I jej królestwo!
Pepper|17|True|I magia Zombiah!
Pepper|18|False|Musimy ją ostrzec!
Rat|19|False|Na platformie czeka na nas smoczy pilot. Tak się wydostaniemy.
Carrot|20|False|No, zamek w końcu puścił.
Rat|21|False|Czing!
Sound|1|True|Brawo!
Sound|2|True|Już biorę kapelusz.
Sound|3|True|piiiii!
Sound|4|True|PRYCH!
Sound|5|False|piiiiiiii!
Pepper|22|False|! ! !
Shichimi|23|False|! ! !
Guard|24|True|STRAŻE!
Guard|25|True|DO MNIE!
Guard|26|False|INTRUZ UWALNIA WIĘŹNIA!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|A było tak blisko.
Pepper|2|False|To prawda.
Pepper|3|False|Czy wiesz, dlaczego Wasabi tak się na mnie uwzięła?
Shichimi|4|True|Boi się wiedźm Chaosu, Pepper.
Shichimi|5|True|A szczególnie waszej reakcji łańcuchowej.
Shichimi|6|False|Wierzy, że jesteście poważnym zagrożeniem dla jej planów.
Pepper|7|True|Ach, jeśli tak,
Pepper|8|False|nie ma się, o co bać. Nigdy nie udało mi się takowej wywołać.
Shichimi|9|False|Naprawdę?
Pepper|10|False|Tak, hahaha!
Shichimi|11|True|Hehe!
Shichimi|12|False|Straszna paranoiczka z niej.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Oficerze!
King|2|False|Czy możecie potwierdzić, że ta świą-tynia należy do wiedźmy?
Officer|3|True|Najprawdopo-dobniej, mój panie!
Officer|4|False|Nasi zwiadowcy ostatnio ją tam widzieli.
King|5|True|Grrr...
King|6|False|Zatem zagrożenie dla naszej sztuki wojennej znajduje się właśnie tutaj.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Uczcijmy nasz sojusz odwetem i zmieceniem tej świątyni z powierzchni Herevy.
Sound|2|False|Klap
Enemy|3|False|D obrze powiedziane!
Army|4|True|Tak!
Army|5|True|Taa!
Army|6|True|Tak!
Army|7|True|Do boju!
Army|8|True|Haa!
Army|9|True|Hura!
Army|10|True|Taaaa!
Army|11|True|Hurrrrra!
Army|12|True|KATAPULTY!
Army|13|False|OGNIA!!!
King|14|True|UWOOOOOOOOOOO !
King|15|False|Wziuuuu!
Sound|16|False|Wziiuuuuum!
Sound|17|True|DO ATAKU!!!
Sound|18|False|Co się dzieje?
King|19|False|Atakują?!
Pepper|20|True|Bu M!
Pepper|21|False|Buuu M!
Sound|22|True|Jak to?!
Sound|23|False|Buu~uU M!
Shichimi|24|False|BU U M !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|ekhe!
Pepper|2|False|ekhe!
Pepper|3|False|Shichimi! Nic ci nie jest?
Shichimi|4|True|Wszystko dobrze!
Shichimi|5|True|Trzymacie
Shichimi|6|False|się jakoś?
Pepper|7|False|Nic nam nie jest.
Pepper|8|True|Jak to?
Pepper|9|False|Nie mogę uwierzyć.
Shichimi|10|True|Co to za żołnierze?!
Shichimi|11|True|I katapulty?!
Shichimi|12|False|Czego chcą?
Pepper|13|True|Nie wiem,
Pepper|14|False|ale znam tamtych dwóch .
Shichimi|15|False|?
Shichimi|16|False|! ! !
Shichimi|17|False|Torreya, tutaj!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Duchom niech będą dzięki, nic ci nie jest.
Sound|3|False|Łaps
Torreya|4|True|Tak się marwtiłam, gdy usłyszałam o twoim uwięzieniu
Torreya|5|True|i tej bitwie!
Torreya|6|False|Co za chaos!
Shichimi|7|False|Torreya, jak dobrze cię widzieć.
Pepper|8|True|O kurczę...
Pepper|9|True|Czyli ta smocza pilotka jest dzieczyną Shichimi.
Pepper|10|True|Nie wyobrażam sobie, co stałoby się, gdybym się jej pozbyła.
Pepper|11|True|A te armie musiały za mną podążyć.
Pepper|12|True|Bez nich nie uwolniłabym się.
Pepper|13|False|Wszystko wydaje się być połączone.
Pepper|14|False|OCH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Co z nią?
Shichimi|2|False|Pepper, wszystko gra?
Pepper|3|True|Tak, tak,
Pepper|4|False|Po prostu coś sobie uświadomi-łam.
Pepper|5|True|Wszystko, co się wydarzyło, jest, bezpośrednio lub pośrednio, wyni-kiem moich działań,wyborów.
Pepper|6|False|Innymi słowy - reakcji łańcuchowej!
Shichimi|7|True|Naprawdę?
Shichimi|8|False|Będziesz mu-siała to wytłu-maczyć.
Torreya|9|True|Starczy gadania, jesteśmy w środku bitwy.
Torreya|10|True|Porozmawiamy, lecąc.
Torreya|11|False|Wskakuj, Pepper!
Shichimi|12|True|Torreya ma rację.
Shichimi|13|False|Bezwzględnie musimy lecieć do Qualicity.
Pepper|14|False|Chwilkę.
Pepper|15|True|Armia Wasabi za moment odpowie atakiem.
Pepper|16|True|Nie możemy pozwolić na rzeź.
Pepper|17|False|Czuję się odpowiedzialna za zakończenie tej bitwy.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Ale jak?
Arra|2|True|Dokładnie, jak tego dokonasz, wiedźmo?
Arra|3|False|Nie zregenerowałaś jeszcze Rei, czuję to.
Pepper|4|True|To prawda, ale mam takie zaklęcie, które może wszystko naprawić.
Pepper|5|False|Będę potrzebować twojej Rei, by zadziałało.
Arra|6|True|Przekazać energię wiedźmie?!
Arra|7|True|To zakazane!
Arra|8|False|Nic z tego!
Pepper|9|False|Wolisz patrzeć, jak się mordują?
Torreya|10|True|Arra, proszę, zrób wyjątek. Te dziewczyny i smoki to nasza rodzina.
Torreya|11|False|Twoja też.
Shichimi|12|False|Miej litość.
Arra|13|True|PFF! Dobra!
Arra|14|False|Ale na jej wła-sne ryzyko!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|fs z z z z! ! !
Pepper|2|False|ŁAAAAA!
Pepper|3|False|Więc to taka jest smocza Rea.
Shichimi|4|False|Pepper, pospiesz się!
Pepper|5|True|Allus...!
Pepper|6|True|Yuus...!
Pepper|7|True|Needum...!
Pepper|8|True|Est
Pepper|9|False|LOVIUS!
Sound|10|False|Dzziuuu!!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Fiizz!
Sound|2|True|Dziing!
Sound|3|True|Hfhii!
Sound|4|True|Szczii!
Sound|5|True|Szczsss!
Sound|6|True|Fiizz!
Sound|7|True|Dziing!
Sound|8|True|Sczii!
Sound|9|True|Hfheee!
Sound|10|False|Sczsss!
Pepper|11|True|To moje pierwsze zaklęcie antywojenne.
Pepper|12|True|Zamienia śmiertel-nych wrogów w przy-jaciół,
Pepper|13|False|a przemoc w miłość i współczucie.
Shichimi|14|True|Łał!
Shichimi|15|True|To wspaniałe, Pepper!
Shichimi|16|False|Przestali walczyć!
Torreya|17|False|Udało się!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Co tam się dzieje?
Shichimi|2|False|Oni się całują?!
Torreya|3|True|No, to przybyło nam par.
Torreya|4|False|To jeden z efektów, Pepper?
Pepper|5|False|Ależ skąd. Wydaje mi się, że smocza Rea wzmocniła skutki zaklęcia.
Torreya|6|False|Jestem pewna, że ta bitwa zostanie dokładnie opisana w podręcznikach.
Shichimi|7|True|Haha, masz to, jak w banku!
Shichimi|8|False|Zaraz spłonę ze wstydu!
Pepper|9|False|- KONIEC -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|15 grudnia, 2021 Rysunki i scenariusz: David Revoy. Poprawki skryptu: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. Wersja polska Tłumaczenie: Sölve Svartskogen. Korekta i kontrola jakości: Besamir. Oparto na uniwersum Herevy Autor: David Revoy. Pomocnik: Craig Maloney. Pisarze: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekta: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0β, Inkscape 1.1 na Kubuntu Linux 20.04. Licencja: Creative Commons Uznanie Autorstwa 4.0. www.peppercarrot.com
Pepper|2|False|Wiedziałeś, że
Pepper|3|False|Pepper&Carrot jest całkowicie darmowy, liberalny, open-source'owy oraz wspierany przez naszych czytelników?
Pepper|4|False|Za ten odcinek dziękujemy 1036 patronom!
Pepper|5|False|Ty również możesz zostać patronem Pepper&Carrot i znaleźć się na tej liście.
Pepper|6|False|Jesteśmy na Patreonie, Tipeee, PayPal, Liberapay i wielu innych!
Pepper|7|False|Wejdź na www.peppercarrot.com, by dowiedzieć się więcej!
Pepper|8|False|Dziękujemy!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
