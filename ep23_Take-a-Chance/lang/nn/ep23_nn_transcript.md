# Transcript of Pepper&Carrot Episode 23 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 23: Grip sjansen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|Komona marknad, ei veke seinare
Skrift|2|False|Kjendis
Skrift|3|False|50 000 Ko
Skrift|4|False|Safrans suksess
Skrift|5|False|Mote
Skrift|6|False|Fenomenet Safran
Skrift|7|False|Chic
Skrift|8|False|Safran-spesial
Lyd|9|False|P A FF !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Takk, Gulrot ...
Pepar|2|False|Kvar einaste dag strevar eg for å vera ei god heks ...
Pepar|3|True|... som respekterer tradisjonane og reglane.
Pepar|4|False|Det kjennest urettferdig at Safran vert kjendis ved å bruka slike triks.
Pepar|5|False|Verkeleg ikkje rett.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suksessånda|2|True|God dag!
Suksessånda|3|False|La meg presentera meg:
Lyd|1|False|Klong!|nowhitespace
Suksessånda|4|False|Eg er suksessånda, til teneste tjuefire timar i døgnet, sju dagar i veka!*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suksessånda|1|False|* Gjeld ikkje helger eller offentlege fridagar. Kan ikkje kombinerast med andre tilbod. Gjeld så lenge lageret rekk.
Suksessånda|2|False|Marknadsføring over heile verda på ein blunk!
Suksessånda|4|False|Godt omdøme internasjonalt!
Suksessånda|3|False|Frisør og stylist følgjer med!
Suksessånda|5|False|Garantert resultat med det same!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suksessånda|1|False|Berre ein liten signatur på denne kontrakten, så vil døra til heider og ære stå vidopen for deg!
Pepar|2|False|Vent no litt!
Pepar|4|True|Kva for deus ex machina - opplegg er det her?
Pepar|3|False|Du kjem ramlande ned frå skyene med ei mirakelløysing på problema mine, nett no når alt er som verst?
Pepar|5|False|Det er mistenkjeleg, er det. Veldig mistenkjeleg!
Pepar|6|True|Me går, Gulrot!
Pepar|7|False|Me hadde kan henda falle for noko slikt før, men så lettlurte er me heldigvis ikkje lenger.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fugl|1|True|Kurr!
Fugl|2|True|Kurr!
Fugl|3|True|Kurr!
Fugl|4|False|Kurr!
Pepar|5|False|Veit du, Gulrot, eg trur det er slik det er å verta vaksen.
Pepar|6|False|Å unngå snarvegar. Å verdsetja det å gjera ein innsats.
Pepar|7|False|Det har ikkje noko føre seg å vera misunneleg på at andre gjer det bra!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|Kjendis
Skrift|2|False|50 000 Ko
Skrift|3|False|Safrans suksess
Skrift|4|False|Mote
Skrift|5|False|Fenomenet Safran
Pepar|8|False|Ein dag vil lukka smila til oss òg.
Skrift|6|False|Chic
Skrift|7|False|Safran-spesial
Lyd|9|False|Bsooom!
Skrift|10|False|Kjendis
Skrift|11|False|Fr. Dues jetsettliv
Skrift|12|False|Mote
Skrift|13|False|Fenomenet fr. Due
Skrift|14|False|Chic
Skrift|15|False|Fr. Due-spesial
Forteljar|16|False|– SLUTT –

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|August 2017 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Arild Torvund Olsen og Karl Ove Hufthammer
Bidragsytarar|2|False|Korrektur og forbetring av dialogen: Alex Gryson, Calimeroteknik, Nicolas Artance, Valvin og Craig Maloney.
Bidragsytarar|4|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Bidragsytarar|5|False|Programvare: Krita 3.1.4 og Inkscape 0.92dev på Linux Mint 18.2 Cinnamon
Bidragsytarar|6|False|Lisens: Creative Commons Attribution 4.0
Bidragsytarar|8|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
Bidragsytarar|7|False|Pepar og Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 879 som støtta denne episoden:
Bidragsytarar|3|False|Hjelp med biletmanus og iscenesetjing: Calimeroteknik og Craig Maloney.
