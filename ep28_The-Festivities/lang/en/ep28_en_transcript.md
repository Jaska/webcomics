# Transcript of Pepper&Carrot Episode 28 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 28: The Festivities

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|True|The three moons of Hereva were in alignment that night,
Narrator|2|False|and their reflected light caused quite a spectacle in the Zombiah cathedral.
Narrator|3|False|It was under this magical light that Coriander, my friend, became...
Narrator|4|False|Queen of Qualicity.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|The festivities began soon after.
Narrator|2|False|A giant party with all of the magic schools and hundreds of prestigious guests from all over Hereva.
Pepper|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Oh! It's you!
Pepper|3|False|I was deep in my own thoughts.
Carrot|2|False|rub rub
Pepper|4|False|I'm going inside. It's getting cold out here.
Journalist|6|False|Hurry! She's here!
Journalist|5|False|Quick! It's her!
Journalist|7|False|Miss Saffron! Can you say a few words for the Qualicity Daily?
Saffron|8|False|Yes, of course!
Pepper|15|False|...
Pepper|16|False|You see, Carrot? I think I understand why I'm not in the mood.
Sound|9|False|F LASH|nowhitespace
Journalist|13|True|Miss Saffron! Hereva Style Gazette.
Sound|10|False|F LASH|nowhitespace
Sound|11|False|F LASH|nowhitespace
Sound|12|False|F LASH|nowhitespace
Journalist|14|False|Which designer are you wearing tonight?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|All my friends are so successful and sometimes I wish I had just a fraction of what they have.
Pepper|2|False|Coriander is a queen.
Pepper|3|False|Saffron is a rich superstar.
Pepper|4|False|Even Shichimi looks so perfect with her school.
Pepper|5|False|And me? What do I have?
Pepper|6|False|This.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Pepper?
Shichimi|2|False|I snuck out some food. Do you want any?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|It feels weird that I have to sneak around to eat any food.
Shichimi|2|False|But our teacher tells us we must look like pure spirits with no basic human needs.
Saffron|3|False|Ha! There you are!
Saffron|4|True|Finally! Somewhere I can go to get away from the photographers.
Saffron|5|False|They drive me crazy.
Coriander|6|False|Photographers?
Coriander|7|False|Try avoiding boring discussions with politicians when you have this thing on your head.
Sound|8|False|scratch scratch

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|By the way, Pepper, I finally met your godmothers.
Coriander|2|False|They really are “special”.
Pepper|3|False|!!!
Coriander|4|True|I mean, you are so lucky.
Coriander|5|False|I'm sure they let you do whatever you want.
Coriander|6|False|Like, defying this useless posturing and banter without worrying about diplomacy.
Saffron|7|False|Or dancing and having fun without caring what people might think.
Shichimi|8|False|Or tasting all the food at the buffet without fearing that someone will see you!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|4|False|TO BE CONTINUED...
Shichimi|1|False|Oh! Pepper!? Did we say something to upset you?
Pepper|2|True|Not at all!
Pepper|3|False|Thank you, my friends!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|3|True|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 960 patrons!
Pepper|7|True|Check www.peppercarrot.com for more info!
Pepper|6|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|8|False|Thank you!
Pepper|2|True|Did you know?
Credits|1|False|January, 2019 Art & scenario: David Revoy. Beta readers: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. English version Proofreading: CalimeroTeknik, Craig Maloney, Martin Disch. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.1.5~appimage, Inkscape 0.92.3 on Kubuntu 18.04.1. License: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
