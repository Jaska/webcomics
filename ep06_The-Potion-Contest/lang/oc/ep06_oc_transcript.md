# Transcript of Pepper&Carrot Episode 06 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 6 : Lo concors de pocion

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Macarèl, soi encara anada dormir sens barrar la fenèstra...
Pepper|2|True|...mas qual vent !
Pepper|3|False|...e perqué vesi Komona per la fenèstra ?
Pepper|4|False|KOMONA !
Pepper|5|False|Lo concors de Pocion !
Pepper|6|True|Ai... probable que me soi endormida sens me'n mainar
Pepper|9|True|... mas ?
Pepper|10|False|Ont soi aquí ?!?
Aucèl|12|False|aC ?|nowhitespace
Aucèl|11|True|co|nowhitespace
Pepper|7|False|*
Nòta|8|False|* véser episòdi 4 : Treslús d'engèni

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carròt ! Siás tròp manhac d'aver pensat a me menar al concors !
Pepper|3|False|Me-ra-vi-lhós !
Pepper|4|True|As quitament pensat a préner una pocion, mos vesits e mon capèl...
Pepper|5|False|...gaitam un pauc quala pocion as presa...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|QUÉ ?!!
Cònsol de Komona|3|False|Declari, coma Cònsol de Komona, lo concors de pocions obèrt !
Cònsol de Komona|4|False|Nòstra vila es plan contenta d'aculhir, per aquesta primièra edicion, QUATRE mascas !
Cònsol de Komona|5|True|Mercé d'aplaudir
Cònsol de Komona|6|False|plan fòrt :
Escritura|2|False|Concors de Pocions de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audiéncia|29|False|Clap
Cònsol de Komona|1|True|Venguda del grand país de l'Union dels Tecnologistas, es un onor d'aculhir la polida e engenhosa
Cònsol de Komona|3|True|...sens doblidar la filha del país, la masca de Komona meteis
Cònsol de Komona|5|True|...nòstra tresena participanta nos ven del país de las lunas colcas
Cònsol de Komona|7|True|... enfin, nòstra darrièra participanta, qu'arriba de la forèst del Cap de l'Esquiròl
Cònsol de Komona|2|False|Coriandre !
Cònsol de Komona|4|False|Safran !
Cònsol de Komona|6|False|Shichimi !
Cònsol de Komona|8|False|Pepper !
Cònsol de Komona|9|True|Que lo concors comence !
Cònsol de Komona|10|False|Lo vòte se farà a l'aplaudimètre !
Cònsol de Komona|11|False|E primièr, plaça a la demostracion de Coriandre !
Coriandre|12|False|Dònas e sénhers...
Coriandre|13|True|...i a pas pus besonh d'aver paur de la mòrt mercés a...
Coriandre|14|True|.. ma Pocion de
Coriandre|15|False|ZOMBIFICACION !
Audiéncia|16|True|Clap
Audiéncia|17|True|Clap
Audiéncia|18|True|Clap
Audiéncia|19|True|Clap
Audiéncia|20|True|Clap
Audiéncia|21|True|Clap
Audiéncia|22|True|Clap
Audiéncia|23|True|Clap
Audiéncia|24|True|Clap
Audiéncia|25|True|Clap
Audiéncia|26|True|Clap
Audiéncia|27|True|Clap
Audiéncia|28|True|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|FANTASTIC!
Cònsol de Komona|2|False|Clap
Audiéncia|4|True|Clap
Audiéncia|5|True|Clap
Audiéncia|6|True|Clap
Audiéncia|7|True|Clap
Audiéncia|8|True|Clap
Audiéncia|9|True|Clap
Audiéncia|10|True|Clap
Audiéncia|11|True|Clap
Audiéncia|12|True|Clap
Audiéncia|13|True|Clap
Audiéncia|14|True|Clap
Audiéncia|15|True|Clap
Audiéncia|16|True|Clap
Audiéncia|17|False|que vaquí
Safran|19|True|...mas enfin, gardatz vòstres aplaudiments, pòble de Komona !
Safran|18|False|La vertadièra pocion qu'esperàvetz totes : la que pòt impressionar totes vòstres vesins...
Safran|22|True|... los rendre geloses !
Safran|23|False|MA
Safran|20|True|BORGESIÁ !
Safran|26|False|... pocion de
Safran|25|True|...tot aquò es ara possible mercés a l'aplicacion d'una soleta gota de ma...
Safran|24|True|Clap
Audiéncia|28|True|Clap
Audiéncia|29|True|Clap
Audiéncia|30|True|Clap
Audiéncia|31|True|Clap
Audiéncia|32|True|Clap
Audiéncia|33|True|Clap
Audiéncia|34|True|Clap
Audiéncia|35|True|Clap
Audiéncia|36|True|Clap
Audiéncia|37|True|Clap
Audiéncia|38|True|Clap
Audiéncia|39|True|Clap
Audiéncia|40|True|Clap
Audiéncia|42|False|Clap
Audiéncia|41|True|aquela pocion poiriá rendre tot Komona ric !
Cònsol de Komona|44|False|Fantastic ! Incredible !
Cònsol de Komona|43|True|Clap
Audiéncia|46|True|Clap
Audiéncia|47|True|Clap
Audiéncia|48|True|Clap
Audiéncia|49|True|Clap
Audiéncia|50|True|Clap
Audiéncia|51|True|Clap
Audiéncia|52|True|Clap
Audiéncia|53|True|Clap
Audiéncia|54|True|Clap
Audiéncia|55|True|Clap
Audiéncia|56|True|Clap
Audiéncia|57|True|Clap
Audiéncia|58|True|Clap
Audiéncia|59|True|Clap
Audiéncia|60|False|Clap
Audiéncia|3|True|Clap
Audiéncia|27|True|Vòstres aplaudiments nos enganan pas, Coriandre es ja eliminada !
Cònsol de Komona|45|False|Pocion
Safran|21|False|Coriandre desfisa la mòrt meteissa amb aquela pocion mi-ra-cu-lo-sa !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|Serà ara malaisit de rivalizar per Shichimi !
Shichimi|4|True|NON !
Shichimi|5|True|pòdi pas, es tròp dangierós
Shichimi|6|False|PERDON !
Cònsol de Komona|3|False|... anem Shichimi, tot lo monde vos espèra
Cònsol de Komona|7|False|Semblariá, Dònas e Sénhers, que Shichimi se retire...
Safran|8|False|Balha-me aquò !
Safran|9|False|...puèi arrèste de far de la timida e arroïnes pas l'espectacle
Safran|10|False|Tot lo monde sap ja qu'ai ganhat lo concors, alara qué que ta pocion faga...
Shichimi|11|False|!!!
Son|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|MOSTRE GIGANT !
Shichimi|2|False|Eee... sabiái pas que caliá far una demostracion.
Shichimi|13|True|ATENCION !!!
Shichimi|14|True|Es una pocion de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aucèl|1|False|CRi CRi CRiiii ii ii ii ii ii ii iii i ii|nowhitespace
Son|2|False|BAM !
Pepper|3|True|...ben, cool !
Pepper|5|False|...ma pocion deuriá almens aver lo meriti de vos far rire que...
Pepper|4|False|es doncas mon torn ara ?
Cònsol de Komona|6|True|Vai-te'n, piòta !
Cònsol de Komona|7|False|lo concors es acabat ! Salva ta vida !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...coma de costuma, tot lo monde se'n va al moment qu'es nòstre torn
Pepper|1|True|E vaquí :
Pepper|4|True|En tot cas ai una petita idèa de çò qu'anam poder far amb ta « pocion » Carròt
Pepper|5|False|...fagam un pauc de recapte aquí e tornam a l'ostal !
Pepper|7|True|Tu lo
Pepper|8|False|Piu-piu-gigant-zòmbi-borgés !
Pepper|10|False|T'agradariá de testar la darrièra pocion ?
Pepper|11|False|... pas tant qu'aquò, è ?
Pepper|6|False|ÈP !
Son|9|False|C R AC !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Òc, legís plan l'etiqueta...
Pepper|2|False|... esitarai pas a te'n versar dessús se te rambas pas sul pic de Komona !
Cònsol de Komona|3|True|Perque a salvat nòstra vila d'un desastre,
Cònsol de Komona|4|False|atribuissèm la primièra plaça a Pepper per sa pocion de... ??!!
Pepper|7|False|...bè... en fait, es pas vertadièrament una pocion ; son los escapolons d'urina de mon gat per sa visita medicala !
Pepper|6|True|... Haha ! òc ...
Pepper|8|False|...pas de demo è ?...
Narrator|9|False|Episòdi 6 : Lo concors de pocion
Narrator|10|False|FIN
Escritura|5|False|50 000 Ko
Crèdits|11|False|Març de 2015 - Dessenh e Scenari : David Revoy , correccions : Aurélien Gâteau

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat dels lectors. Per aqueste episòdi, mercé als 245 mecènas :
Crèdits|4|False|https://www.patreon.com/davidrevoy
Crèdits|3|False|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent :
Crèdits|7|False|Otisses : Aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita sus Linux Mint
Crèdits|6|False|Open source : totas las sorsas, polissas d'escrituras, fichièrs amb calques son disponibles sul site oficial al telecargament.
Crèdits|5|False|Licéncia : Creative Commons Attribution podètz modificar, tornar partejar, vendre, etc...
Crèdits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
