# Transcript of Pepper&Carrot Episode 06 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Epizoda 6: Tekmovanje v napojih
<hidden>|2|False|Korenček

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Presneto, že spet sem zaspala pri odprtem oknu ...
Pepper|2|True|... toliko vetra ...
Pepper|3|False|... in kako to, da vidim Komono skozi okno?
Pepper|4|False|KOMONA!
Pepper|5|False|Tekmovanje v napojih!
Pepper|6|False|Mogoče sem... mogoče sem po nesreči zaspala!
Pepper|9|True|... ampak?
Pepper|10|False|Kje pa sem ?!?
Bird|12|False|ga?|nowhitespace
Bird|11|True|ga|nowhitespace
Note|7|False|* glej epizodo 4: Napad genialnosti

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Korenček! Tako lepo, da si se spomnil in me pripeljal na tekmovanje!
Pepper|3|False|Fan-tas-tično !
Pepper|4|True|Celo spomnil si se prinesti napoj, moja oblačila in moj klobuk ...
Pepper|5|False|... pa poglejmo kateri napoj si prinesel ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|KAJ ?!!
Mayor of Komona|3|False|Kot župan Komone, razglašam da se tekmovanje v napojih ... začenja!
Mayor of Komona|4|False|Našemu mestu je v veliko čast, da pri prvi prireditvi tega tekmovanja pozdravljamo nič manj kot štiri čarovnice.
Mayor of Komona|5|True|Prosim za
Mayor of Komona|6|True|velik
Writing|2|False|Komonsko tekmovanje v napojih
Mayor of Komona|7|False|aplavz za :

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Plosk
Mayor of Komona|1|True|Pripotovala je iz oddaljene Zaveze tehnologov, zato nam je v čast izreči dobrodošlico prikupni in veleumni
Mayor of Komona|3|True|... da ne pozabimo naše lastne čarovnice iz Komone,
Mayor of Komona|5|True|... naša tretja tekmovalka k nam prihaja iz dežele zahajajočih lun,
Mayor of Komona|7|True|... in končno še zadnja tekmovalka, iz gozda pri Veveričjem koncu,
Mayor of Komona|2|False|Coriander !
Mayor of Komona|4|False|Saffron !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|Naj se igre pričnejo!
Mayor of Komona|10|False|Glasovalo se bo z aplavzemerom.
Mayor of Komona|11|False|Prva na vrsti za demostracijo je Coriander.
Coriander|13|False|... ne bojte se več smrti, zahvaljujoč mojemu ...
Coriander|14|True|... Napoju
Coriander|15|False|ZOMBIFIKACIJE !
Audience|16|True|Plosk
Audience|17|True|Plosk
Audience|18|True|Plosk
Audience|19|True|Plosk
Audience|20|True|Plosk
Audience|21|True|Plosk
Audience|22|True|Plosk
Audience|23|True|Plosk
Audience|24|True|Plosk
Audience|25|True|Plosk
Audience|26|True|Plosk
Audience|27|True|Plosk
Audience|28|True|Plosk
Coriander|12|False|Dame in Gospodje...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTIČNO !
Audience|3|True|Plosk
Audience|4|True|Plosk
Audience|5|True|Plosk
Audience|6|True|Plosk
Audience|7|True|Plosk
Audience|8|True|Plosk
Audience|9|True|Plosk
Audience|10|True|Plosk
Audience|11|True|Plosk
Audience|12|True|Plosk
Audience|13|True|Plosk
Audience|14|True|Plosk
Audience|15|True|Plosk
Audience|16|False|Plosk
Saffron|18|True|ker tukaj je
Saffron|17|True|... ampak prosim, da še zadržite velik aplavz, dragi Komonci !
Saffron|22|False|... jih naredil ljubosumne!
Saffron|19|True|MOJ
Saffron|25|False|RAZKO Š JA !
Saffron|24|True|... napoja
Saffron|23|False|... vse to je mogoče z nanosom ene same kapljice mojega ...
Audience|26|True|Plosk
Audience|27|True|Plosk
Audience|28|True|Plosk
Audience|29|True|Plosk
Audience|30|True|Plosk
Audience|31|True|Plosk
Audience|32|True|Plosk
Audience|33|True|Plosk
Audience|34|True|Plosk
Audience|35|True|Plosk
Audience|36|True|Plosk
Audience|37|True|Plosk
Audience|38|True|Plosk
Audience|39|True|Plosk
Audience|40|False|Plosk
Mayor of Komona|42|False|Ta napoj bi lahko prinesel bogastvo celi Komoni!
Mayor of Komona|41|True|Fantastično! Neverjetno !
Audience|44|True|Plosk
Audience|45|True|Plosk
Audience|46|True|Plosk
Audience|47|True|Plosk
Audience|48|True|Plosk
Audience|49|True|Plosk
Audience|50|True|Plosk
Audience|51|True|Plosk
Audience|52|True|Plosk
Audience|53|True|Plosk
Audience|54|True|Plosk
Audience|55|True|Plosk
Audience|56|True|Plosk
Audience|57|True|Plosk
Audience|58|True|Plosk
Audience|59|True|Plosk
Audience|60|False|Plosk
Mayor of Komona|2|False|Coriander pretenta sámo smrt s tem ču-do-de-lnim napojem!
Saffron|21|True|Pravi napoj, na katerega ste vsi čakali: takšnega ki bo osupnil vase sosede ...
Mayor of Komona|43|False|Vaš aplavz se ne moti. Coriander je že izločena.
Saffron|20|False|Napoj

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Kot kaže bo zdaj zadnjo demonstracijo Shichimi težko premagala !
Shichimi|4|True|NE !
Shichimi|5|True|Ne morem, prenevarno je.
Shichimi|6|False|OPROSTITE !
Mayor of Komona|3|False|... daj no Shichimi, vsi čakamo nate
Mayor of Komona|7|False|Kot kaže, dame in gospodje, da Shichimi odstopa...
Saffron|8|False|Daj mi to !
Saffron|9|False|... in nehaj se pretvarajati da si sramežljiva, kvariš predstavo.
Saffron|10|False|Vsem je že jasno, da sem jaz zmagala to tekmovanje, ne glede na to kaj tvoj napoj zmore ...
Shichimi|11|False|!!!
Sound|12|False|Z Z Z U U UP|nowhitespace
Shichimi|15|False|OGROMNE PO Š ASTI !
Shichimi|2|False|N... Nisem vedela, da jih moramo demonstrirati.
Shichimi|13|True|PREVIDNO!!!
Shichimi|14|True|To je napoj za

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KRA-KRA-Kr a aa aa aa a a aa|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|... ha, super !
Pepper|5|False|... moj napoj bo vsaj smeha vreden, zato ker ...
Pepper|4|False|torej sem jaz na vrsti ?
Mayor of Komona|6|True|Beži, butara!
Mayor of Komona|7|False|Tekmovanja je konec ! ... Vsi na varno!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... kot ponavadi, vsi oddidejo ravno ko sva midva na vrsti.
Pepper|1|True|A vidiš to ...
Pepper|4|True|Zdaj vsaj imam idejo, kaj lahko storim s tvojim "napojem", Korenček.
Pepper|5|False|... postavim vse nazaj v red, in nato proti domu!
Pepper|7|True|Ti
Pepper|8|False|Prevelik-luksuz-zombi-kanarček!
Pepper|10|False|A želiš preizkusiti še zandji napoj? ...
Pepper|11|False|... raje ne, a ?
Pepper|6|False|HEJ !
Sound|9|False|K R AAK !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Tako ja, pozorno preberi oznako ...
Pepper|2|False|... ne bom se obotavljala preden tole polijem vsepovsod po tebi, če takoj ne izgineš iz tega mesta !
Mayor of Komona|3|True|Ker je rešila naše mesto pred veliko nevarnostjo.
Mayor of Komona|4|False|Podelimo prvo mesto Pepper za njen napoj ... ??!!
Pepper|7|False|... am ... v bistu to sploh ni napoj. To je vzorec urina mojega mačka od zadnjega obiska pri veterinarju!
Pepper|6|True|... Haha! ja ...
Pepper|8|False|... raje brez predstavitve, kajne ?...
Narrator|9|False|Epizoda 6: Tekmovanje v napojih
Narrator|10|False|KONEC
Writing|5|False|50,000 Ko
Credits|11|False|Marec 2015 - Umetniško delo in zgodba od Davida Revoya - Prevedel Andrej Ficko

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot je popolnoma prost, odprtokoden, ter podprt in financiran s pomočjo bralcev. Za to epizodo se zahvaljujem 245 sponzorjem :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Tudi ti lahko postaneš sponzor Pepper&Carrot za naslednjo epizodo :
Credits|7|False|Orodja : Ta epizoda je bila 100% izrisana s prostim programjem Krita na Linux Mint
Credits|6|False|Odprta koda : vsa izvorna koda s sloji in pisavami je na voljo na uradni strani
Credits|5|False|Licenca : Creative Commons Attribution Dovoljeno spreminjanje, deljenje, prodajanje, itd...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
