# Transcript of Pepper&Carrot Episode 04 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 4: Glimt av genialitet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|Klokka 2, natta før trylledrikk-konkurransen
Pepar|2|True|Endeleg ... Ikkje eingong Safran kan laga noko som kan måla seg med denne trylledrikken!
Pepar|3|False|No treng eg berre ein prøvekanin ...
Pepar|4|False|GUUUUUUUUL-ROOOOT!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|14|False|KATTEMAT
Pepar|1|False|Gulrot?
Pepar|2|False|Utruleg! Og ikkje under senga heller?
Pepar|4|False|Gulrot?
Pepar|5|False|Gulrot?!
Pepar|6|False|Gulrot?!
Pepar|3|False|Gulrot!
Pepar|7|False|Er det verkeleg slik at eg må ty til dette knepet ...?
Pepar|8|False|Gulrot!
Lyd|9|True|Kirrr
Lyd|10|True|Kirrr
Lyd|11|True|Kirrr
Lyd|12|True|Kirrr
Lyd|13|False|Kirrr
Lyd|15|True|Rasla
Lyd|16|True|Rasla
Lyd|17|True|Rasla
Lyd|18|True|Rasla
Lyd|19|False|Rasla
Lyd|21|False|Rasla Rasla Rasla Rasla Rasla
Lyd|20|False|Kirrr Kirrr Kirrr Kirrr Kirrr
Lyd|23|False|Kirrr Kirrr Kirrr Kirrr Kirrr
Lyd|24|False|Rasla Rasla Rasla Rasla Rasla
Gulrot|25|False|Rumla
Lyd|26|False|Pffjoooo!
Gulrot|22|False|O sole mjaaau!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Ååå, søte, vesle Gulrot! Kom du heilt av deg sjølv?
Pepar|2|False|Akkurat idet eg hadde bruk for favoritt-assistenten min.
Pepar|3|True|Her ser du meisterverket mitt ...
Pepar|4|False|Eit geni-brygg!
Pepar|5|False|Berre ein liten slurk ...
Pepar|6|False|... og så skal du få oppleva eit glimt av genialitet.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Plukk
Pepar|3|False|Oi! Gulrot! Det er utruleg ...
Pepar|5|False|Du ... du skriv ...
Pepar|6|False|E?
Pepar|7|False|Energi?
Pepar|8|False|Evig?
Pepar|9|False|Emosjon?
Lyd|2|False|Skrrrrrrr
Lyd|4|False|Skrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Kva anna kan det tyda?
Pepar|2|False|Emne?
Pepar|3|False|Empati?
Pepar|4|False|Kom igjen, Gulrot! Eg kjem ikkje på fleire ord. Det gjev verkeleg inga meining!
Pepar|5|False|... det gjev verkeleg inga meining ...
Pepar|6|True|Grrrrrrr!!!
Pepar|7|False|Aldri får eg til noko!
Pepar|8|True|Ja ja ...
Pepar|9|False|Eg har framleis tid til å finna opp ein annan trylledrikk.
Lyd|10|False|KN...UUUUS!
Pepar|11|False|Sov godt, Gulrot!
Forteljar|12|False|Framhald følgjer ...
Bidragsytarar|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|3|False|Алексей – 獨孤欣 – Adrian Lord – Alan Hardman – Albert Westra – Alejandro Flores Prieto – Alexander Sopicki – Alex Lusco Alex Silver – Alex Vandiver – Alfredo – Alice Queenofcats – Ali Poulton (Aunty Pol) – Alison Harding – Allan Zieser Andreas Rieger – Andrew – Andrew Godfrey – Andrey Alekseenko – Angela K – Anna Orlova – anonymous – Antan Karmola Arco – Ardash Crowfoot – Arne Brix – Aslak Kjølås-Sæverud – Axel Bordelon – Axel Philipsenburg – barbix – Ben Evans Bernd – Boonsak Watanavisit – Boudewijn Rempt – Brian Behnke – carlos levischi – Charlotte Lacombe-bar – Chris Radcliff Chris Sakkas – Christophe Carré – Clara Dexter – Colby Driedger – Conway Scott Smith – Cyrille Largillier – Cyril Paciullo Daniel – david – Damien de Lemeny – Dmitry – Donald Hayward – Dubema Ulo – Eitan Goldshtrom – Enrico Billich-epsilon-– Eric Schulz – Garret Patterson – Guillaume – Gustav Strömbom – Guy Davis – Happy Mimic – Helmar Suschka Ilyas Akhmedov – Inga Huang – Irene C. – Ivan Korotkov – Jared Tritsch – JDB – Jean-Baptiste Hebbrecht – Jessey Wright John – Jónatan Nilsson – Jon Brake – Joseph Bowman – Jurgo van den Elzen – Justin Diehl – Kai-Ting (Danil) Ko – Kasper Hansen Kathryn Wuerstl – Ken Mingyuan Xia – Liselle – Lorentz Grip – MacCoy – Magnus Kronnäs – Mandy – marcus – Martin Owens Masked Admirer – Mathias Stærk – mattscribbles – Maurice-Marie Stromer – mefflin ross bullis-bates – Michael Gill Michelle Pereira Garcia – Mike Mosher – Morten Hellesø Johansen – Muzyka Dmytro – Nazhif – Nicholas DeLateur Nick Allott – Nicki Aya – Nicolae Berbece – Nicole Heersema – Noah Summers – Noble Hays – Nora Czaykowski Nyx – Olivier Amrein – Olivier Brun – Omar Willey – Oscar Moreno – Öykü Su Gürler – Ozone S. – Paul – Paul Pavel Semenov – Peter – Peter Moonen – Petr Vlašic – Pierre Geier – Pierre Vuillemin – Pranab Shenoy – Rajul Gupta Ret Samys – rictic – RJ van der Weide – Roman – Rumiko Hoshino – Rustin Simons – Sami T – Samuel Mitson – Scott RussSean Adams – Shadefalcon – Shane Lopez – Shawn Meyer – Sigmundur Þórir Jónsson – Simon Forster – Simon Isenberg – Sonny W. – Stanislav Vodetskyi – Stephen Bates – Steven Bennett – Stuart Dickson – surt – Tadamichi Ohkubo – tar8156TheFaico – Thomas Schwery – Tracey Reuben – Travis Humble – Vespertinus – Victoria – Vlad Tomash – Westen CurryXavier Claude – Yalyn Vinkindo – og ei særskild takk til Amireeti for hjelp med korrekturlesing på engelsk!
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne opne kjelder, og er sponsa av lesarane. Takk til dei 156 som støtta denne episoden:
Bidragsytarar|6|False|https://www.patreon.com/davidrevoy
Bidragsytarar|5|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot:
Bidragsytarar|10|False|Verktøy: Denne episoden er 100 % teikna med fri programvare: Krita, G’MIC, Blender og Gimp på Ubuntu Gnome (Gnu/Linux).
Bidragsytarar|9|False|Opne kjelder: Du kan lasta ned alle kjeldefilene, med tilhøyrande skriftfiler, på den offisielle heimesida.
Bidragsytarar|8|False|Lisens: Creative Commons Attribution (til «David Revoy»). Du kan byggja vidare på episoden, dela han, selja han osb.
Gulrot|4|False|Tusen takk!
Bidragsytarar|7|False|Omsetjing til nynorsk: Arild Torvund Olsen og Karl Ove Hufthammer.
