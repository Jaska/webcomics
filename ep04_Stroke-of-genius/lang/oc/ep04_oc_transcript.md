# Transcript of Pepper&Carrot Episode 04 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 4 : Treslús d'engèni

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|La velha del concors de pocion, 2h00 del matin ...
Pepper|2|True|Enfin... Amb aquesta pocion degun , quitament pas Safran, m'anarà pas a la cavilha !
Pepper|3|False|Ai sonque besonh de la testar...
Pepper|4|False|CAAAAAAAA~ ~AAARRÒT !!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|14|False|GAT MIAM
Pepper|1|False|Carròt ?
Pepper|2|False|Incredible !... a mai pas jol lièit ?
Pepper|4|False|Carròt ?
Pepper|5|False|Carròt ?!
Pepper|6|False|Carròt ?!!!
Pepper|3|False|Carròt !
Pepper|7|False|Pensavi pas ne dever venir aquí e utilizar aquela tecnica...
Pepper|8|False|Carròt !
Son|9|True|Crrr
Son|10|True|Crrr
Son|11|True|Crrr
Son|12|True|Crrr
Son|13|False|Crrr
Son|15|True|Chhh
Son|16|True|Chhh
Son|17|True|Chhh
Son|18|True|Chhh
Son|19|False|Chhh
Son|21|False|Chhh Chhh Chhh Chhh Chhh
Son|20|False|Crrr Crrr Crrr Crrr Crrr
Son|23|False|Crrr Crrr Crrr Crrr Crrr
Son|24|False|Chhh Chhh Chhh Chhh Chhh
Carròt|25|False|Gargblr
Son|26|False|Zoooooooo !
Carròt|22|False|O Sole Miauuuoo !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Òòòòò, Carròt ! siás tròp manhac d'èstre vengut d'espertu
Pepper|2|False|Exactament al moment qu'ai besonh de mon assistent favorit
Pepper|3|True|Taaadaaaa ! Te presenti mon cap d'òbra !
Pepper|4|False|la pocion dels engènis
Pepper|5|False|sonque una petita gorjada...
Pepper|6|False|...e deuriás èstre trucat per un treslús d'engèni

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Pòc
Pepper|3|False|òu... Carròt ! es meravilhós...
Pepper|5|False|e... ... escrives ?
Pepper|6|False|E ?
Pepper|7|False|Energia ?
Pepper|8|False|Eternal ?
Pepper|9|False|Emocion ?
Son|4|False|Chrrrrrrr
Son|2|False|Chrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Qué ?
Pepper|2|False|Embrumat ?
Pepper|3|False|Emanar ?
Pepper|4|False|Anèm Carròt, fai esfòrces, vòl pas res dire tot aquò
Pepper|5|False|...vòl pas res dire tot aquò...
Pepper|6|True|Grrrrrrr !!!
Pepper|7|False|Res fonciona pas jamai amb ieu !
Pepper|8|True|Basta ...
Pepper|9|False|me demòra encara de temps per ne preparar una mai bona
Son|10|False|CLASChchChchChh !
Pepper|11|False|...bona nuèit Carròt ...
Narrator|12|False|de seguir...
Crèdits|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo e de mercejaments especials a Amireeti per m'aver ajudat amb las correccions anglesas !
Crèdits|1|False|Pepper&Carrot es completament liure, open source e esponsorizat mercés al mecenat dels lectors, per aqueste episòdi, mercé als 156 mecènas :
Crèdits|6|False|https://www.patreon.com/davidrevoy
Crèdits|5|False|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent :
Crèdits|9|False|Otisses : Aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita, G'MIC, Blender, GIMP sus Ubuntu Gnome (GNU/Linux)
Crèdits|8|False|Open-source : totas las fonts, polissas d'escritura, fichièrs amb calques son disponibles sul site oficial al teledescargament.
Crèdits|7|False|Licéncia : Creative Commons Atribucion a 'David Revoy' podètz modificar, tornar partatjar, vendre, etc...
Crèdits|4|False|Mercé !
