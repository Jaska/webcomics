# Transcript of Pepper&Carrot Episode 03 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 3 : L'ingredient secret

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Nòta|4|False|* Ko = unitat monetària de Komona
Narrator|1|False|Vila de Komona, Jorn de mercat
Pepper|2|False|Adissiatz sénher, volriái uèit cogordestelas, vos prègui !
Vendaire|3|False|Vaquí, còsta 60Ko*
Pepper|5|False|zut ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Eee soi desolada, ne vau pas prene que quatre fin finala
Vendaire|2|False|Grrr...
Safran|3|False|Adissiatz sénher, doas detzenas de cadun, sens vos comandar. De qualitat superiora, coma de costuma.
Vendaire|4|False|Es totjorn un plaser de vos servir, Madomaisèla Safran.
Safran|5|False|Tè ! Gaitatz-me aquò ! Es Pepper !
Safran|6|False|Me digas pas, los afars marchan tras que plan a la campanha ?
Pepper|7|False|...
Safran|8|False|Imagini que preparas ja tos ingredients pel concors de pocions de deman ?
Pepper|9|True|... un concors de pocions ?
Pepper|10|False|... deman ?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Qual bonastre aquel concors ! E ai encara tot un jorn per m'i preparar
Pepper|5|False|Solide que vau participar !
Escritura|1|False|Concors de Pocions de Komona
Escritura|2|False|grand prèmi de 50 000 Ko PER LA MELHORA POCION
Escritura|3|False|Diazard, 3h del dèxtre Grand plaça de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Òu ... ! sabi ! ...
Pepper|3|False|... es exactament çò que me cal !
Pepper|4|True|Carròt !
Pepper|5|False|prepara-te, partissèm a la caça als ingredients !
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Primièr, me calriá de pèrlas de brumas de nívols negras...
Pepper|2|False|... e qualques granas rojas de la sèlva malefica.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|... puèi qualques clòscas d'uòus de fènix de la Valada dels volcans ...
Pepper|2|False|... E fin finala qualques gotas de lait d'una jova Dragonvaca

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|I sèm Carròt, pensi qu'avèm tot çò que cal
Pepper|2|True|Me sembla...
Pepper|3|False|... perfièit
Pepper|4|True|Mmm ...
Pepper|5|True|Melhor ... cafè ... del monde !
Pepper|6|False|Es çò que me caliá abans de trabalhar tota la nuèit per far la melhora pocion pel concors de deman
Narrator|7|False|de seguir ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Crèdits|2|False|Aqueste episòdi poiriá pas existir sens lo sosten de 93 mecènas
Crèdits|1|False|Aqueste webcomic es completament liure e open-source (Creative Commons Attribution 3.0, fichièrs nauta resolucion disponibles al telecargament)
Crèdits|3|False|https://www.patreon.com/davidrevoy
Crèdits|6|False|Mercejament especial Amireeti, David Tschumperlé (G'MIC) e tota la « còla Krita » !
Crèdits|7|False|Aqueste episòdi foguèt realizat a 100% amb d'otisses liures, Krita e G'MIC sus Xubuntu (GNU/Linux)
Pepper|5|False|Mila mercés !
