# Transcript of Pepper&Carrot Episode 05 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 5: Julespesial

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|2|False|jul
Skrift|1|True|God

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 170 som støtta denne episoden:
Bidragsytarar|4|False|https://www.patreon.com/davidrevoy
Bidragsytarar|3|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot:
Bidragsytarar|7|False|Verktøy: Denne episoden er 100 % teikna med fri programvare: Krita, G’MIC og Blender på Ubuntu Gnome (Gnu/Linux).
Bidragsytarar|6|False|Opne kjelder: Du kan lasta ned alle kjeldefilene, med tilhøyrande skriftfiler, på den offisielle heimesida.
Bidragsytarar|5|False|Lisens: Creative Commons Attribution (til «David Revoy»). Du kan byggja vidare på episoden, dela han, selja han osb.
Bidragsytarar|2|False|Addison Lewis – Adrian Lord – Alan Hardman – Albert Westra – Alejandro Flores Prieto – Aleš Tomeček – Alexander Bülow Tomassen – Alexander Sopicki – Alex Lusco – Alex Silver – Alex Vandiver – Alfredo – Ali Poulton (Aunty Pol) – Allan Zieser – Andreas Rieger – Andrej Kwadrin – Andrew – Andrew Godfrey – Andrey Alekseenko – Angela K – Anna Orlova – anonymous – Antan Karmola – Ardash Crowfoot – Arne Brix – Aslak Kjølås-Sæverud – Axel Bordelon – Axel Philipsenburg – barbix – Ben Evans – Bernd – Boonsak Watanavisit – Boudewijn Rempt – Brett Smith – Brian Behnke – carlos levischi – Charlotte Lacombe-bar – Chris Radcliff – Chris Sakkas – Christophe Carré – Christopher Bates – Clara Dexter – Colby Driedger – Conway Scott Smith – Cyrille Largillier – Cyril Paciullo – Damien – Daniel – Daniel Björkman – david – Dmitry – Donald Hayward – Eitan Goldshtrom – Enrico Billich –-epsilon-– Eric Schulz – Frederico – Garret Patterson – GreenAngel5 – Guillaume – Gustav Strömbom – Guy Davis – Happy Mimic – Helmar Suschka – Ilyas Akhmedov – Inga Huang – Irene C. – Ivan Korotkov – James Frazier – Jared Tritsch – JDB – Jean-Baptiste Hebbrecht – Jeffrey Schneider – Jessey Wright – John – John – Jónatan Nilsson – Jonathan Leroy – Jon Brake – Joseph Bowman – Jurgo van den Elzen – Justin Diehl – Kai-Ting (Danil) Ko – Kasper Hansen – Kathryn Wuerstl – Ken Mingyuan Xia – Liselle – Lorentz Grip – MacCoy – Magnus Kronnäs – Mandy Streisel – marcus – Martin Owens – Mary Brownlee – Masked Admirer – Mathias Stærk – mattscribbles – Maurice-Marie Stromer – mefflin ross bullis-bates – Michael Gill – Michelle Pereira Garcia – Mike Mosher – Morten Hellesø Johansen – Muzyka Dmytro – Nazhif – Nicholas DeLateur Nick Allott – Nicki Aya – Nicola Angel – Nicolae Berbece – Nicole Heersema – Noble Hays – Nora Czaykowski – Nyx – Olivier Amrein – Olivier Brun – Omar Willey – Oscar Moreno – Öykü Su Gürler – Ozone S. – Paul – Paul – Pavel Semenov – Peter – Peter Moonen – Petr Vlašic – Pierre Geier – Pierre Vuillemin – Pranab Shenoy – Rajul Gupta – Ret Samys – rictic – RJ van der Weide – Roberto Zaghis – Roman – Rumiko Hoshino – Rustin Simons – Sally Bridgewater – Sami T – Samuel Mitson – Scott Russ – Sean Adams Shadefalcon – Shane Lopez – Shawn Meyer – Sigmundur Þórir Jónsson – Simon Forster – Simon Isenberg – Sonny W. – Stanislav Vodetskyi – Stephen Bates – Steven Bennett – Stuart Dickson – surt – Tadamichi Ohkubo – tar8156 – TheFaico – Thomas Schwery – Tracey Reuben – Travis Humble – tree – Vespertinus – Victoria – Vlad Tomash – Westen Curry – Xavier Claude – Yalyn Vinkindo – Алексей – Глеб Бузало – 獨孤欣 & 獨弧悦
