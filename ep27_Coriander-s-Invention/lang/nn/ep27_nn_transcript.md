# Transcript of Pepper&Carrot Episode 27 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 27: Oppfinninga til Koriander

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|Ettermiddag i byen Kvalistad.
Skreddar|4|False|Ver så snill , prinsesse Koriander ...
Skreddar|5|False|Kan du ikkje prøva å stå litt stille?
Koriander|6|False|Greitt, greitt ...
Skreddar|7|False|Me har knapt med tid – kronings-seremonien er om berre tre timar.
Koriander|8|False|...
Skrift|2|True|KRONINGA AV HENNAR MAJESTET
Skrift|3|False|DRONNING KORIANDER
<hidden>|0|False|Translate “Her Majesty’s Coronation” and “Queen Coriander” on the banner on the building (in the middle). The other text boxes will update automatically (they are clones).
<hidden>|0|False|NOTE FOR TRANSLATORS
<hidden>|0|False|You can strech the scroll and translate “Qualicity” to “City of Qualicity”.
<hidden>|0|False|NOTE FOR TRANSLATORS

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Du ...
Koriander|2|False|Eg går og grublar over tryggleiken igjen ...
Koriander|3|True|Det er så mange kongar og dronningar som har blitt snikmyrda under kronings-seremonien.
Koriander|4|False|Også i slekta mi.
Koriander|5|True|Og om berre nokre timar får me tusenvis av gjestar frå heile verda – og eg vert midtpunktet.
Pepar|7|True|Ta det roleg, Koriander.
Koriander|6|False|Det gjer meg skikkeleg redd!
Pepar|8|True|Me følgjer planen vår.
Pepar|9|False|Shichimi og eg skal vera livvaktene dine, veit du.
Shichimi|10|True|Akkurat.
Shichimi|11|False|Ta ein pause no – og tenk på noko anna ei stund.
Koriander|12|False|OK.
Koriander|13|False|Forresten – kan eg visa dykk den siste oppfinninga mi?
Skreddar|14|False|...
Koriander|15|True|Eg har henne i arbeidsrommet i kjellaren.
Koriander|16|False|Denne vegen.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|False|Det er ein spesiell grunn til at eg ville laga henne.
Koriander|2|False|Det heng saman med uroa mi rundt kroninga.
Koriander|3|False|Hels på den nye oppfinninga ...
Psykolog-bot|5|False|Hei, verda.
Psykolog-bot|6|False|Eg er din nye PSYKOLOG-BOT. Blipp, blopp!
Psykolog-bot|7|False|Legg deg ned på divanen for ei gratis undersøking.
Lyd|4|False|Klikk!
Lyd|8|False|Klapp!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|No, kva synest du?
Koriander|2|False|Lyst til å prøva han?
Pepar|3|True|Pøh.
Pepar|4|True|Trengst ikkje.
Pepar|5|False|Eg har ingen psykiske vanskar.
Lyd|6|True|B|nowhitespace
Pepar|11|False|KVA ER DET SOM FEILAR DEN ROBOTEN DIN?!
Koriander|12|True|Orsak!
Koriander|13|True|Han har framleis ein liten feil eg ikkje har klart å retta enno.
Koriander|14|False|Han gjev automatisk støyt når han oppdagar ei løgn .
Pepar|15|False|Ei løgn?!
Pepar|16|False|Men eg har ikkje fortalt ei løgn i heile mitt LIV!
Pepar|22|True|DETTE ...
Pepar|23|True|ER IKKJE ...
Pepar|24|False|MOROSAMT!!!
Lyd|7|True|Z|nowhitespace
Lyd|8|True|Z|nowhitespace
Lyd|9|True|Z|nowhitespace
Lyd|10|False|!|nowhitespace
Lyd|17|True|B
Lyd|18|True|Z|nowhitespace
Lyd|19|True|Z|nowhitespace
Lyd|20|True|Z|nowhitespace
Lyd|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Morosamt? Nei, sjølvsagt ikkje! Orsak.
Koriander|2|False|He-he! Heilt sant! Slett ikkje morosamt!
Pepar|8|True|Ha ...
Pepar|9|True|Ha ...
Pepar|10|False|Ha.
Koriander|11|True|Å, NEI!
Koriander|12|False|Kroningsdrakta er øydelagd! Skreddaren vert rasande!
Koriander|13|True|Slem robot!
Koriander|14|False|Eg skal retta feilen din, ein gong for alle!
Pepar|15|False|VENT!!!
Lyd|3|True|B
Lyd|4|True|Z|nowhitespace
Lyd|5|True|Z|nowhitespace
Lyd|6|True|Z|nowhitespace
Lyd|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Eg trur eg veit korleis me kan dra nytte av denne feilen.
Lyd|2|False|Pling!
Forteljar|3|False|Same kveld ...
Koriander|11|False|Du er heilt genial, Pepar!
Pepar|12|True|Nei, det er det du som er.
Pepar|13|False|Det er jo di oppfinning!
Psykolog-bot|14|False|Ver så god, neste.
Psykolog-bot|15|True|Hei.
Psykolog-bot|16|False|Har du planlagt eit attentat i kveld?
Forteljar|17|False|FRAMHALD FØLGJER ...
Lyd|6|True|B
Lyd|7|True|Z|nowhitespace
Lyd|8|True|Z|nowhitespace
Lyd|9|True|Z|nowhitespace
Lyd|10|False|!|nowhitespace
Skrift|4|True|KRONINGA AV HENNAR MAJESTET
Skrift|5|False|DRONNING KORIANDER

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 1 060 som støtta denne episoden!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
Pepar|2|True|Visste du dette?
Bidragsytarar|1|False|31. oktober 2018 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire og Zveryok. Korrekturlesing engelsk versjon: Calimeroteknik, Craig Maloney, Midgard og Martin Disch . Omsetjing til nynorsk: Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson . Programvare: Krita 4.2-dev og Inkscape 0.92.3 på Kubuntu 18.04. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
