# Transcript of Pepper&Carrot Episode 34 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 34: Riddarseremoni for Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|Same kveld ...
Hibiskus|2|False|... og i kveld kan me dermed ynskja deg, Shichimi, velkomen som den yngste riddaren av ah.
Koriander|3|False|Pepar har enno ikkje dukka opp?
Safran|4|True|Nei.
Safran|5|False|Ho må skunda seg. No går ho glipp av talen til Shichimi ...
Shichimi|6|True|Tusen takk.
Shichimi|7|False|Eg vil gjerne ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Zio oOOOOO|nowhitespace
Lyd|2|False|Zioo O O O OO|nowhitespace
Pepar|3|True|LØØØYPE!
Pepar|4|True|Unna veg!
Pepar|5|False|UNNA VEG!!!
Lyd|6|False|KR A S J!|nowhitespace
Pepar|7|True|Oi sann!
Pepar|8|False|Gjekk det bra? Ingen brotne bein?
Shichimi|9|False|Pepar!
Pepar|10|True|Hei, Shichimi!
Pepar|11|True|Orsak for den dramatiske entréen og for at eg er så sein!
Pepar|12|False|Eg har sprunge i heile kveld – men det er ei lang historie.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiskus|1|False|Er ho ein av gjestane dine?
Shichimi|2|False|Ja, det er veninna mi Pepar – alt i orden.
Pepar|3|True|Gjekk det bra, Gulrot?
Pepar|4|False|Lei for det der. Eg har enno ikkje heilt fått dreisen på å landa med hyperfart.
Pepar|5|True|Igjen, orsak til alle for bryet ...
Pepar|6|False|... og for kleda mine ...
Shichimi|7|False|Tji-hi!
Wasabi|8|True|Shichimi,
Wasabi|9|False|denne unge heksa som nettopp kom, er ho verkeleg ei veninne av deg?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Ja, dykkar høgvyrde.
Shichimi|2|False|Ho heiter Pepar og høyrer til kaosah-skulen.
Wasabi|3|True|Nærværet hennar skitnar til den heilage skulen vår.
Wasabi|4|False|Få henne bort med det same!
Shichimi|5|True|Men ...
Shichimi|6|False|Meister Wasabi ...
Wasabi|7|True|Men kva ?
Wasabi|8|False|Vil du heller verta utvist frå skulen vår for alltid?
Shichimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Pepar, eg er lei for det, men du må dra.
Shichimi|2|False|No.
Pepar|3|False|Hæ?
Pepar|4|False|Hei, vent no litt! Det her må vera ei misforståing.
Shichimi|5|False|Ver så snill, Pepar, ikkje gjer det vanskeleg.
Pepar|6|False|Hei, du der, på trona! Om du mislikar meg slik, kan du koma ned her og seia det sjølv!
Wasabi|7|False|Tsk-tsk ...
Wasabi|8|False|Du har ti sekund, Shichimi ...
Wasabi|9|False|Ni ...
Wasabi|10|False|Åtte ...
Wasabi|11|False|Sju ...
Shichimi|12|False|DET HELD, PEPAR! DRA!!!
Lyd|13|False|SJRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Shichimi, ro deg n...
Lyd|2|False|B ADuuM!|nowhitespace
Shichimi|3|True|DRA!!!
Shichimi|4|True|DRA!!!
Shichimi|5|False|DRA!!!
Lyd|6|False|K R E E E E E! !!|nowhitespace
Pepar|7|True|Au!
Pepar|8|False|Hei sann! Det ... er i-IKKJE ... AU! ... snilt!
Koriander|9|False|SHICHIMI! PEPAR! HALD OPP!
Safran|10|False|Vent.
Wasabi|11|False|Hm!
Pepar|12|True|Grrr!
Pepar|13|False|Du har sjølv bedt om det!
Lyd|14|False|B R Z OO!!|nowhitespace

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|FORBANNUS KANSELLARE MAXIMUS!!!
Lyd|2|False|S J K L AK!|nowhitespace
Pepar|3|False|Au!
Lyd|4|False|P AFF !!|nowhitespace
Shichimi|5|False|Sjølv ikkje den sterkaste annullerings-formelen din verkar på meg!
Shichimi|6|False|Gje opp og dra, Pepar!
Shichimi|7|False|Ikkje tving meg til å skada deg meir!
Pepar|8|False|Å, formelen verka som han skulle – men du var ikkje målet.
Shichimi|9|True|Hæ?
Shichimi|10|False|Kva meiner du?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepar|2|True|Det var ho som var målet!
Pepar|3|False|Eg annullerte forheksinga som fekk henne til å sjå ung ut!
Pepar|4|False|Eg la merke til denne forheksinga di med det same eg kom.
Pepar|5|False|Så eg gav deg ein liten lærepenge for at du tvinga Shichimi til å slåst mot meg!
Wasabi|6|True|UFORSKAMMA!
Wasabi|7|True|Korleis vågar du,
Wasabi|8|False|og framfor heile skulen min!
Pepar|9|True|Pris deg heller lukkeleg!
Pepar|10|False|Om eg hadde all reaen min, hadde det blitt mykje verre.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|DR Z OW!!|nowhitespace
Wasabi|2|False|Eg forstår. Du kom opp på nivå med forgjengarane dine mykje tidlegare enn eg hadde venta ...
Wasabi|3|False|Det framskundar planane mine, men det er berre bra.
Pepar|4|True|Planane dine?
Pepar|5|False|Du sette meg altså berre på prøve, og alt det her hadde ikkje noko å gjera med at eg kom for seint?
Pepar|6|False|Du er verkeleg ikkje heilt god ...
Wasabi|7|True|Tji-|nowhitespace
Wasabi|8|False|hi.|nowhitespace
Wasabi|9|True|KVA ER DET DE GLOR PÅ?!
Wasabi|10|False|EG VART NETTOPP FORULEMPA, MEN DE BERRE STÅR DER OG KOPER?! TA HENNE!!!
Wasabi|11|False|FANG HENNE LEVANDE!!!
Wasabi|12|False|TA HENNE!!!
Pepar|13|False|Dette må me snakka om seinare, Shichimi!
Pepar|14|True|Eg er lei for det, Gulrot, men me må nok bruka hyperfart igjen ...
Pepar|15|False|Hald deg fast!
Lyd|16|False|Klapp!
Lyd|17|False|Klapp!
Lyd|18|False|Ziioo OO!!|nowhitespace

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|? !|nowhitespace
Wasabi|2|False|FANG HENNE!!!
Pepar|3|False|Kjære vene.
Safran|4|False|Pepar – bruk limen min!
Lyd|5|False|Fizzz!
Pepar|6|True|Supert!
Pepar|7|False|Takk, Safran!
Lyd|8|False|Grip!
Lyd|9|False|zioo O O O O!|nowhitespace
Forteljar|10|False|FRAMHALD FØLGJER ...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 1 096 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
Bidragsytarar|1|False|31. mars 2021 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore og Valvin. Omsetjing til nynorsk Karl Ove Hufthammer og Arild Torvund Olsen. Ei særskild takk til Nartance som utforska Wasabi-karakteren i sine eigne forteljingar. Måten han skildra henne på, hadde sterk innverknad på korleis eg har framstilt henne i denne episoden. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson . Programvare: Krita 4.4.1 og Inkscape 1.0.2 på Kubuntu Linux 20.04. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
