#!/bin/bash

# Script for validating that Inkscape hasn't generated file:// links

echo "##############################################################"
echo "Searching SVG files for file:/// links"
echo " "

# Move up if we're not in the base directory.
if [ -d "../.ci" ]; then
    pushd ..
fi

FILE_LINKS=$(grep "file:///" * -r --include \*.svg  --include \*.SVG)
if ! [[ -z ${FILE_LINKS} ]] ; then
    while IFS= read -r line ; do
        echo "$line"
    done < <(printf '%s\n' "$FILE_LINKS")
    echo " "
    echo "##############################################################"
    echo "ERROR: Found file:/// link(s) - please fix the files above."
    echo "File links should look like this:"
    echo xlink:href="../gfx_Pepper-and-Carrot_by-David-Revoy_<panel>.png"
    echo "##############################################################"
    exit 1
else
    echo "Done, all clean!"
    echo "##############################################################"
    exit 0
fi
