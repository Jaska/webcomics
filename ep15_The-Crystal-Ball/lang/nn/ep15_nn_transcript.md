# Transcript of Pepper&Carrot Episode 15 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 15: Krystallkula

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|– SLUTT –
Bidragsytarar|2|False|Mars 2016 – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 686 som støtta denne episoden:
Bidragsytarar|2|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot:
Bidragsytarar|3|False|https://www.patreon.com/davidrevoy
Bidragsytarar|4|False|Lisens: Creative Commons Attribution 4.0 Kjeldefiler: Tilgjengelege på www.peppercarrot.com Verktøy: Denne episoden er 100 % teikna med fri programvare Krita 2.9.11, Inkscape 0.91 på Linux Mint 17
