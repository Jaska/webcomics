# Transcript of Pepper&Carrot Episode 10 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 10: Wumanuth

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dedidehá|1|False|- NODI -
Dohiná|2|False|08/2015 - Alehala i woban: David Revoy - Héedan: Yuli i Álo

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|1|False|https://www.patreon.com/davidrevoy
Dohiná|2|True|Bíi thad naden ne Loyud i Ámed beth www.patreon.com/davidrevoy beha wa
Dohiná|3|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth. Áala withedim 422 hi:
Dohiná|4|False|Hudi: Creative Commons Attribution 4.0 Bíi ham woban www.peppercarrot.com beha Bodibod: Bíi wudeth hi thodeshub wodódin wobodibodenan wa Krita 2.9.6, Inkscape 0.91, Linux Mint 17 beha
