# Transcript of Pepper&Carrot Episode 10 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 10: Sommerspecialudgave

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|- Slut -
Credits|2|False|08/2015 - Design & manuskript: David Revoy

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 422 tilhængere :
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|2|True|Støt næste episode af Pepper&Carrot; hver en krone gør en forskel:
Credits|4|False|Licens: Creative Commons Kreditering 4.0 Kildematerialet: tilgængelige på www.peppercarrot.com Værktøj: denne episode er 100% designet med fri software: Krita 2.9.6, Inkscape 0.91 på Linux Mint 17
