# Transcript of Pepper&Carrot Episode 20 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 20: Yod Ranil

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dedidehá|1|False|- NODI -
Dohiná|2|False|12/2016 - www.peppercarrot.com - Alehala i Woban: David Revoy
Dohiná|3|False|Bíi nosháad dedide thera Shebedoni bethude wa. El David Revoy beth. Den Craig Maloney. Dódóon Willem Sonke, Moini, Hali, CGand i Alex Gryson.
Dohiná|4|False|Hudi: Creative Commons Attribution 4.0, Bodibod: Krita 3.1, Inkscape 0.91, Manjaro XFCE beha

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|1|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth. Áala withedim 825 hi:
Dohiná|2|False|Bíi thad naden ne Loyud i Ámed beth www.patreon.com/davidrevoy beha wa
