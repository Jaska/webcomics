# Transcript of Pepper&Carrot Episode 37 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 37: Les Larmes du Phénix

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah...
Pepper|2|False|Ça fait du bien d'être arrivée !
Pepper|3|False|Tiens, c'est le jour du marché.
Pepper|4|False|On devrait sans doute manger un bout avant l'ascension du volcan, non ?
Pepper|5|False|J'étais sûre que l'idée allait te plaire !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|1|False|AVIS DE RECHERCHE
Écriture|2|False|Torreya
Écriture|3|False|1 00 000Ko
Écriture|4|False|Shichimi
Écriture|5|False|250 000Ko
Écriture|6|False|Pepper
Écriture|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ils ont vraiment mis ça partout...
Pepper|2|False|Même dans les coins les plus reculés.
Pepper|3|False|Allez, filons avant d'éveiller des soupçons.
Pepper|4|False|On a des problèmes bien plus importants pour le moment...
Pepper|5|False|Cette lumière...
Pepper|6|False|On doit être proches de son nid.
Pepper|7|False|Bingo !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phénix|1|False|Pourquoi viens-tu me déranger, humaine ?
Pepper|2|False|Salutations, Ô grand Phénix !
Pepper|3|False|Je m'appelle Pepper, et je suis une sorcière de Chaosah.
Pepper|4|False|J'ai récemment reçu une énorme dose de Réa de dragon, et depuis, j'ai cette chose qui est apparue et qui progresse de jour en jour...
Pepper|5|False|Ça neutralise tous mes pouvoirs et risque à terme de me tuer.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phénix|1|True|Hmm...
Phénix|2|False|Je vois...
Pepper|3|False|...et te voici à présent devant moi, car tu veux des larmes de Phénix pour pouvoir guérir, pas vrai ?
Pepper|4|False|Oui, c'est ma seule option .
Phénix|5|False|*soupir*
Phénix|6|False|... eh bien, qu'attends-tu ?
Phénix|7|False|Essaie donc de me faire pleurer.
Phénix|8|False|Je te donne une minute !
Pepper|9|False|Quoi ?!
Pepper|10|False|Vous faire pleurer ?!
Pepper|11|False|Mais je ne savais pas que...
Pepper|12|False|En une minute , en plus ?!
Pepper|13|True|Heu...
Pepper|14|True|OK !
Pepper|15|False|Voyons voir.
Pepper|16|True|Hmm... Pensez à la faim dans le monde .
Pepper|17|True|Heu non non non !
Pepper|18|True|J'ai mieux :
Pepper|19|False|Vos êtres chers disparus .
Pepper|20|True|Toujours pas ?
Pepper|21|True|Et les animaux abandonnés ?!
Pepper|22|False|Ça, c'est trop triste, les animaux abandonnés...
Phénix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Alors ?
Pepper|2|False|Ça ne vous fait pas pleurer ?
Phénix|3|False|NON MAIS T'ES SÉRIEUSE LÀ ?!!
Phénix|4|False|C'EST TOUT CE QUE TU AS ?!!
Phénix|5|True|Eux, au moins, ont essayé la poésie !
Phénix|6|True|L'écriture de tragédies !
Phénix|7|True|DE L'ART !
Phénix|8|False|DU DRAMATIQUE !
Phénix|9|True|ET TOI ?!
Phénix|10|False|TU VIENS SANS PRÉPARATION !
Phénix|11|True|OUI, C'EST ÇA ; OUSTE !
Phénix|12|False|RENTRE CHEZ TOI, ET REVIENS QUAND TU AURAS TROUVÉ MIEUX !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr ! Allez, Pepper ! Trouve une histoire triste.
Pepper|2|True|Tu peux y arriver.
Pepper|3|False|Tu peux y arriver.
Pepper|4|True|Non...
Pepper|5|False|Tu ne peux pas.
Vendeur|6|False|Hé ! Oh ! Va-t'en !
Pepper|7|False|! !
Vendeur|8|False|T'avise pas d'toucher à ma marchandise si t'as rien pour payer, hein !
Pepper|9|True|Oh non...
Pepper|10|True|CARROT !
Pepper|11|False|Tu pourrais m'aider au lieu de penser à ton ventre !
Pepper|12|False|Oh ?!
Pepper|13|True|Mais oui...
Pepper|14|False|Ça devrait marcher.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phénix|1|True|Ah ! Te revoilà.
Phénix|2|False|C'était rapide...
Phénix|3|False|Une arme en métal ?!
Phénix|4|False|Sérieusement ?!
Phénix|5|False|Ne sais-tu donc pas que je peux faire fondre tous les métaux et...
Son|6|False|Plop
Son|7|False|Plop
Son|8|False|Plop
Phénix|9|True|AH NON !
Phénix|10|False|PAS ÇA !
Phénix|11|False|C'EST PAS DU JEU !
Pepper|12|True|Vite Carrot !
Pepper|13|False|Attrape un max de larmes !
Son|14|False|Coupe !
Son|15|False|Coupe !
Son|16|False|Coupe !
Titre|17|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Le 3 Août 2022 Art & scénario : David Revoy. Lecteurs de la version bêta : Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Version française originale Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 5.0.5appimage, Inkscape 1.2 on Kubuntu Linux 20.04 Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Le saviez-vous ?
Pepper|3|False|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 1058 mécènes !
Pepper|5|False|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|6|False|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|7|False|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|8|False|Merci !
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
