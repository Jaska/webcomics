# Transcript of Pepper&Carrot Episode 31 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 31: Kampen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|Svarthøgda, den heilage åsen til kaosah-heksene.
Lyd|2|False|Vroo om !|nowhitespace
Pepar|3|False|Tsk!
Lyd|4|False|Vro oom!|nowhitespace
Lyd|5|False|Brzooo!
Pepar|6|False|Ta den!
Lyd|8|False|Sjusj!
Kajenne|9|False|Amatør!
Pepar|10|False|! ! !|nowhitespace
Lyd|11|False|BAM!|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Dsjooom!
Lyd|2|False|KNUS! ! !|nowhitespace
Lyd|3|False|KNAS! ! !|nowhitespace
Pepar|4|False|Hå nei, du!
Pepar|5|False|GRAVITASJONAS SKJOLDUS!
Lyd|6|False|Svosj !|nowhitespace
Lyd|7|False|Tsjakk !|nowhitespace
Lyd|8|False|Tsjakk !|nowhitespace
Lyd|9|False|Dunk!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|GULROT!
Pepar|2|False|Plan 7B!
Pepar|4|False|JPEGUS KVALITATIS!
Lyd|5|False|Bsj ooo !|nowhitespace
Lyd|3|False|Zo o om|nowhitespace
Kajenne|6|False|?!!
Kajenne|10|False|Argh!
Lyd|7|True|G
Lyd|8|True|Z|nowhitespace
Lyd|9|False|Z|nowhitespace
Pepar|11|False|KVALITATIS MINIMALIS!
Kajenne|12|False|! ! !|nowhitespace
Kajenne|13|False|Grr...
Skrift|14|False|2019-12-20-E31P03_V15-endeleg.jpg
Skrift|15|False|Feil ved lasting av bilete
Lyd|16|False|KRASJ ! ! !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenne|1|False|PYROBOMBA ATOMIKUS!
Lyd|2|False|Frrrooof!
Lyd|3|True|K
Lyd|4|True|A|nowhitespace
Lyd|5|True|B|nowhitespace
Lyd|6|True|O|nowhitespace
Lyd|7|True|O|nowhitespace
Lyd|8|True|M|nowhitespace
Lyd|9|False|!|nowhitespace
Lyd|10|True|BRRR
Lyd|11|False|BRRR
Lyd|12|False|P s s sj...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenne|1|False|Pfff...
Kajenne|2|True|Flyktar du til ein mikrodimensjon – i din alder?
Kajenne|3|False|For eit patetisk nederlag ...
Pepar|4|True|Feil!
Pepar|5|False|Eit ormehòl!
Lyd|6|False|Bzz!|nowhitespace
Pepar|7|False|Takk for utgangen, Gulrot!
Pepar|8|False|Sjakkmatt, meister Kajenne!
Pepar|9|False|GURGES ...
Pepar|10|False|... ATER!
Lyd|12|False|Ssssssviiiiiiiipppp!!!
Lyd|11|False|V R O OOM ! !!|nowhitespace
Timian|13|False|STOPP!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|1|True|Eg sa STOPP!
Timian|2|False|Det held!
Lyd|3|False|Knips!|nowhitespace
Lyd|4|False|POFF !|nowhitespace
Lyd|5|False|Rist!
Timian|6|True|Kom HIT!
Timian|7|True|NO!
Timian|8|False|Begge to!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Dunk!
Lyd|2|False|Dunk!
Timian|3|True|Kvar einaste gong desse tre åra har du sagt «berre ein siste prøve» ...
Timian|4|False|Me kan ikkje vera her i heile natt!
Timian|5|False|No?
Pepar|6|False|...
Kajenne|7|True|Greitt ...
Kajenne|8|True|Ho kan få vitnemål ...
Kajenne|9|False|... men berre så vidt.
Skrift|10|False|Kaosah-vitnemål
Skrift|13|False|Karve
Skrift|15|False|Kajenne
Skrift|14|False|Timian
Skrift|11|False|~ for Pepar ~
Skrift|12|False|Godkjend heks
Skrift|16|False|– SLUTT –

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|20. desember 2019 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance og Valvin. Omsetjing til nynorsk: Arild Torvund Olsen og Karl Ove Hufthammer . Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson . Programvare: Krita 4.2.6appimage og Inkscape 0.92.3 på Kubuntu 18.04-LTS. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 971 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
