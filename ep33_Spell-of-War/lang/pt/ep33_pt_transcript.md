# Transcript of Pepper&Carrot Episode 33 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episódio 33: Feitiço da Guerra

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Inimigo|1|False|HuuuUUOOOOOOOOO !
Exército|2|False|Rarrrr!
Exército|5|False|Grrarr!
Exército|4|False|Grrrr!
Exército|3|False|Izztzuurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|True|Ok, jovem bruxa
Rei|2|False|Se você tem um feitiço que pode nos ajudar, é agora ou nunca!
Pepper|3|True|Entendido!
Pepper|4|False|Testemunhe...
Som|5|False|Dzzz ! !|nowhitespace
Pepper|6|False|... A MINHA OBRA-PRIMA!!
Som|7|False|DzZZZ ! !|nowhitespace
Pepper|8|False|Realitas Hackeris Pepperus!
Som|9|False|DzZUUU ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|False|Fiizz!
Som|2|False|Dzii!
Som|3|False|Chiii!
Som|4|False|Ffhii!
Som|8|False|Dziing!
Som|7|False|Fiizz!
Som|6|False|Chiii!
Som|5|False|Ffhii!
Rei|9|True|Então este feitiço fortalece as nossas armas...
Rei|10|False|...e enfraquece as dos inimigos?
Som|11|False|Dzii...
Pepper|12|True|Hehe.
Pepper|13|True|Você verá!
Pepper|14|False|Eu só posso dizer que você não perderá nenhum soldado hoje.
Pepper|15|False|Mas você ainda terá que lutar e dar o seu melhor!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|False|É para isso mesmo que nós treinamos.
Rei|2|False|PREPARAAAAAAAAAAAR!
Exército|3|False|ÉÉÉEe!
Exército|4|False|ÉÉée!
Exército|5|False|Ééée!
Exército|6|False|Éééeee!
Exército|7|False|Éééée!
Exército|8|False|Éééé!
Exército|9|False|Éééé!
Exército|10|False|ÉÉÉé!
Exército|11|False|ÉEEeee!
Rei|12|False|ATACAAAAAAAAR!!!
Exército|13|False|ÉÉée!
Exército|14|False|Éeee!
Exército|15|False|Ééé!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|False|Iáááa!!!
Inimigo|2|False|Iuuurr!!!
Som|3|False|Schuinn ! !|nowhitespace
Som|4|False|Swush ! !|nowhitespace
Escrita|5|False|12
Inimigo|6|False|?!!
Escrita|7|False|8
Rei|8|False|?!!
Escrita|9|False|64
Escrita|10|False|32
Escrita|11|False|72
Escrita|12|False|0
Escrita|13|False|64
Escrita|14|False|0
Escrita|15|False|56
Exército|20|False|Irrr!
Exército|17|False|Urr!
Exército|19|False|Grrr!
Exército|21|False|Iáaa!
Exército|18|False|Iéeeee!
Exército|16|False|Iaauu!
Som|27|False|Schuing
Som|25|False|Fffchh
Som|22|False|Uiizz
Som|23|False|Swush
Som|24|False|Clong
Som|26|False|Cling

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|False|?!
Escrita|2|False|3
Escrita|3|False|24
Escrita|4|False|38
Escrita|5|False|6
Escrita|6|False|12
Escrita|7|False|0
Escrita|8|False|5
Escrita|9|False|0
Escrita|10|False|37
Escrita|11|False|21
Escrita|12|False|62
Escrita|13|False|27
Escrita|14|False|4
Rei|15|False|! !|nowhitespace
Rei|16|False|BRUXAAAAAA!!!! O QUE VOCÊ FEZ?!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa !
Pepper|2|True|“Isso"
Pepper|3|False|...é a minha obra-prima!
Pepper|4|False|É um feitiço complexo que altera a realidade e mostra os pontos de vida restantes do oponente.
Escrita|5|False|33
Pepper|6|True|Quando você não tem mais pontos de vida, você sai do campo de batalha.
Pepper|7|True|O primeiro exército que conseguir remover todos os oponentes ganha!
Pepper|8|False|Simples assim.
Escrita|9|False|0
Exército|10|False|?
Pepper|11|True|Não é ótimo?
Pepper|12|True|Sem mais mortes!
Pepper|13|True|Sem mais soldados feridos!
Pepper|14|False|Isso vai revolucionar a guerra do modo que a conhecemos!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Agora que todos nós sabemos as regras, vou resetar os pontos de todos para começarmos de novo.
Pepper|2|False|Assim é mais justo.
Escrita|3|False|64
Escrita|4|False|45
Escrita|5|False|6
Escrita|6|False|2
Escrita|7|False|0
Escrita|8|False|0
Escrita|9|False|0
Escrita|10|False|0
Escrita|11|False|9
Escrita|12|False|5
Escrita|13|False|0
Escrita|14|False|0
Escrita|15|False|0
Som|16|False|Sshing!
Som|17|False|Zooo!
Som|19|False|Tcac!
Som|18|False|Poc!
Pepper|20|True|Corre, Carrot!
Pepper|21|False|O feitiço não vai durar muito!
Narrador|22|False|- FIM -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Créditos|4|False|29 de junho de 2020 Arte e roteiro: David Revoy. Leitores beta: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Versão em Português Tradução: Alexandre E. Almeida. Baseado no universo de Hereva Criador: David Revoy. Mantenedor principal: Craig Maloney. Escritores: Craig Maloney, Nartance, Scribblemaniac, Valvin. Corretores: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.3, Inkscape 1.0 no Kubuntu 19.10. Licença: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Você sabia?
Pepper|6|True|Pepper&Carrot é totalmente livre, com código aberto e patrocinada graças à gentil contribuição dos seus leitores.
Pepper|7|False|Para este episódio, agradeçemos a 1190 patronos!
Pepper|8|True|Você também pode se tornar um(a) patrono(a) de Pepper&Carrot e ter o seu nome aqui!
Pepper|9|True|Nós estamos no Patreon, Tipeee, PayPal, Liberapay ...e mais!
Pepper|10|True|Acesse www.peppercarrot.com para mais informações!
Pepper|11|False|Obrigada!
