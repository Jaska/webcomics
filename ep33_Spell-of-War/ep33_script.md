# Episode 33: Spell of War

In general, this page mirror the first page of the previous episode of Pepper&Carrot (ep32) but this time on the other side of the battlefield. We start on a panorama shot on the other side of the hill and we discover creatures looking like evil muscular greens orcs with long curved swords, maces and spears. Their hair are deep black, their skin are greenish, their clothes are mostly made of fur and leather, they have many tatoos too. They are very impressive warriors. The leader is easy to spot: he sits on a big wild boar. He has in hand a massive horn and he is ready to blow into it. General weather condition is now worst than in the previous episode: the sky has dark clouds.

Shot with on foreground the leader blowing in the horn while in the background the army starts engaging in the fight, yelling with all weapons in hands. It must look aggressive and threatening.

	SOUNFX (horn)
	huuUUUUUUUUUUUURRRRRRRRRRRRRRR!

	ORCS (Yell sound ideas)
	YUuuuurrr! Grrrr! Rawwwrrr! (Snarl?)

Top bird view flying above the orc army 'shoulder' that spreads threw the grassy valley like a hive of bees. In distance (they still have a long minute to react, the distance is long) we can see the silhouettes of the army of the King.

---

Wide shot on the other side of the hill, with our main characters from previous episode: the King in front (Officer is gone), Pepper and a sample of warrior in background. All character focus on the horizon seriously now: warriors and King have now their swords in hands, ready to fight. Pepper kept the same appearance than at the end of ep32 but folded back her hoodie revealing her haircut. We can guess this characters had talk between the two episodes.

(left) Close up on the King still staring at the horizon with intensity.

	KING
	OK, young witch.
	If you have a spell that can help us, it's now or never!

(center) Shot on Pepper very confident with a subtle smile. She prepares spells on her hands glowing in a dark ultra-violet aura.

	PEPPER
	Got it!
	Prepare to witness...

(right) Extreme close up on Pepper now in full dark ultra-violet aura, glowing like a torch.

	PEPPER
	...MY MASTERPIECE!

Large panel with a tiny silhouette of Pepper over a tilted spatial grid as if she was alone on another plan of reality. Long energy wires emits from both her hands with random trajectories (as would do small fireworks). Some of the wire of energy 'hits' things in the void around her and for careful reader this halos draws a subtle outline weapons (swords, spears).

	PEPPER
	Realitas Hackeris Pepperus!

---

Wide panel on all armored soldiers who suddently are surprised by looking at their weapons glowing and being hit with energy.

Wide panel on all the orcs running while also looking at their weapon glowing brightly and being hit by energy.

Close-up on the King, not really understanding what happened, but he looks at his own sword glowing. His eyes also glows, he is very interested and impressed by magic -something he never understood- but respect a lot.

	KING 
	So, does this spell give our swords more power
	and weaken the enemy's weapons?

(left) Low angle shot of Pepper smiling (but a tiny bit exhausted after such a big effort) speaking a bit with the mystery of an oracle. (note: last speechbubbles share a bit of next panel)

	PEPPER
	Heh. You'll see!
	I can say you won't lose any soldiers today.

(center) Close-up on the King, deeply impressed by this sentences and sketch a smile on his face. The idea of not loosing a single soldier boost his confidence.

(right) Extreme close-up on Pepper showing her fist, motivational.

	PEPPER
	But you will still have to fight and give it your best!

---

(left) Close-up of the King smiling at this last part; something he controls! His confidence is boosted and it's enough to convince him. Anyway: time shrinks as enemy do progress now.

	KING
	That is what we have trained to do.

(right) Large shot on the King holding his sword above his head (not glowing anymore, or subtle) yelling. His horse is also in a epic position, standing on his back legs. 

	ON MY COMMAAAAAND!

Wide close-up of the King yelling:

	KING
	...CHARRRGEEEEEE!!!

	SOLDIERS (various)
	Yeaaahhh!! Yaaaahhhh!! Yeeehhhaaaaah!!!

Bird view shot on the valley; hundreds mens are swarming from both side, they only are few meters away to draw the first blood and convert this two groups into a single melee.

---

Side-view as if we were standing in the middle battlefield; one second before the groups of warrior meets.

(left) Action panel of the king ready to use sword in mid-action.

(right) Action panel on the leader of orc ready to use a mace.

Non expected panel where both fighters are very surprised: their weapon went threw the body of their opponent as if the weapons were ghosts. It affected their balance and they both try to retain the movement of their weapons. Big number magically appeared on top of their heads but they haven't noticed yet. In the melee around same effect.

	NUMBER(KING)
	320

	NUMBER(ORC LEADER)
	260

---

Close up on the King looking at his sword, he is not understanding, and starts to sweat of discomfort, a sort of vertigo: this is not normal. This is for him a very uncomfortable and unpredictable situation to can't trust his sword within the heat of the battle. He get anxious.

Large shot on the battlefield fight of soldiers versus orcs soldiers where the King and leader of Orc are both in background looking at their troups fighting around them: we can see the warrior and orcs trying to damage themselves: but same effect for them "ghost weapons" and number appearing above their heads. The leader of the orc still look at his sword maybe a bit slower to understand. (Note: At this moment, the reader should understand obviously the system of video game action RPG with hit points appearing above head of Characters)

	NUMBERS (Random soldiers)
	72 36 54 18 33 0 18 13

Extreme close-up on the King with black eyes covered by shadows. It's like if he understood suddenly something that made him in a black anger.

Shot on the King yelling with all his power to the direction of the battlefield where Pepper is. Soldiers sort of stops around to find, interrupted by this yell.

	KING
	WIIIIIITCCCHHHHHH!!!!
	WHAT HAVE YOU DONE?!!!!

---

Pepper is happily jumping threw the hill to join the place where the fight happens, carrot follows her super happy.

	PEPPER
	♪♫ Tadaaa! ♪♫

(left) Close-up on Pepper super happy and proud of herself, making a little circle with her finger to symbolize "everything":

	PEPPER
	"THIS"
	is my masterpiece!

(center) A large shot of Pepper now near to two fighters. The Orc is trying to hit the soldier, the soldier listen to Pepper at the same time. The blade of the orc goes threw the soldier at the same moment and remaining life points appear above the head of the soldier.

	PEPPER
	It's a complex spell that alters reality and shows your opponent's remaining life points.

	NUMBER(soldier)
	32

A shot of Pepper pushing an orc soldier with a deep red zero number above its head.

	PEPPER
	When you have no more life points, you leave the field for the rest of the battle.
	First army to remove all of the opposing soldiers wins!
	Simple.

Emotional shot of Pepper, she is now looking in distance, projecting her internal fantasy into the future. She think she has an inspirational speech! Her eyes shines! Carrot has hearts in the eyes. Background of the panel can go crazy with colors.

	PEPPER
	Isn't that great?
	This will revolutionize all of the wars!
	No more dead or wounded soldiers!

---

Large view, Pepper is in the center of all warriors, proud of ending her explanation (probably expecting applause, cheering, awards and maybe two Nobel prices) and have hands on her hips. Carrot explodes of joy with a jump of victory misunderstanding how everyone around is looking at Pepper (he is 100% thinking she stopped all the fight by herself, technically sort of right by the way). Warriors around her doesn't understand at all and start to realize slowly what she told them. Some at this moment even drops their weapon understanding this day will be not epic.

Close-up on Pepper pointing a finger on the side of her head as if she reminded something last minute. She winks and smile as if she was proposing a cheat code to a group of gamers.

	PEPPER
	Oh, wait a minute.
	Let me reset the scores so we can start all over again!
	(smaller) It will be more fair.

Close-up on the King and behind him the leader of the orcs. They both look at Pepper (the camera) with the most intensive rage they can have (eyes turns red? background too?).

Last panel takes last half of the page: Pepper runs toward the camera (a bottom right corner of the panel) with Carrot on her side. It's a panic mode run for both to escape a dangerous situation. All soldiers of the two armies are running after them and are throwing their weapons into their directions (a lot of numbers of hit above Pepper's head). Many weapons falls everywhere around them, flying threw them.

	PEPPER
	Run faster Carrot!
	The spell won't last much longer!

	NUMBER(PEPPER, DECREASING FROM YELLOW TO ORANGE TO DEEP DARK RED)
	150 124 88 64 45 32 21 6 0 0 0 0 0 

	FIN

---
